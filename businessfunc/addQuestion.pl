#最终生成题目
use strict;

sub addQuestion
{
	my ($mob_addr,$mobrowser,$accessToken,$addQuestion) = @_;
	#####
	#开始网络请求 点击生成
	#####
	my ($return_json);

	$return_json = $mobrowser->post("$mob_addr/survey-ws/vi/surveysvc/addQuestion",
			{@{$addQuestion}},
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",
	)->content;
	&verification("mob","addQuestion",$mobrowser,$return_json);
	

	return 1;
}
1;