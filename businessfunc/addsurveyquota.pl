#设置配额
#survey/a/addSurveyQuota
#http://10.201.70.81/survey/quotas/create
use strict;

sub addsurveyquota
{
	my ($mgtbrowser,$mgt_addr,$surveyid,$quota_1,$quota_2) = @_;
	
	print "开始创建配额_差异属性\n";
	my (%quota,@quota,$return_json);
	my %head = (
		'Referer'=>"$mgt_addr/survey/quotas/show/$surveyid",
		'Content-Type'=>"application/x-www-form-urlencoded",
	);
	
	if(ref($quota_1) eq "HASH"){
		%quota = %$quota_1;
		$return_json = $mgtbrowser->post("$mgt_addr/survey/quotas/create",{%quota},%head)->content;
	}
	if(ref($quota_1) eq "ARRAY"){
		@quota = @$quota_1;
		$return_json = $mgtbrowser->post("$mgt_addr/survey/quotas/create",{@quota},%head)->content;
	}
	&verification("mgt","addsurveyquota_1",$mgtbrowser,$return_json);

	print "开始创建配额_参与条件\n";
	@quota = @$quota_2;
	$return_json = $mgtbrowser->post("$mgt_addr/survey/a/survey_quota_type",{@quota},%head)->content;
	&verification("mgt","addsurveyquota_2",$mgtbrowser,$return_json);
	
	
	#print "addsurveyquota - $return_json\n";
	#print LOG "addsurveyquota - $return_json\n";
	
}

1;