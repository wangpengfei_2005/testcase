####
#设置逻辑	#管开放平台的逻辑设置
#http://10.201.2.13:8080/survey/a/editLogical
use strict;

sub editLogical
{
	my ($openbrowser,$open_addr,$surveyid,$logicList_arrRef,$questid_arrRef) = @_;
	my (@logicList); 	#逻辑的post数据
	
	
	#预处理公共数据
	push @logicList, "surveyId";
	push @logicList, $surveyid;
	
	#开始从logicList_arrRef中组织数据
	#	"q1-noselect-0 and q2-select-2 to q5",
	#	"q1-select-0 and q2-select-2 or q2-select-2 to q5",
	#	"q1-select-0 and q2-select-2 end",
	for(my $n=0; $n<=$#$logicList_arrRef; $n++){
		my $oneLogic = $logicList_arrRef->[$n];
		#区分跳转逻辑和终止逻辑
		if($oneLogic =~ /to/i){
			push @logicList,"logicalUnits[$n].actionName";
			push @logicList,"JUMP";
		}
		if($oneLogic =~ /end/i){
			push @logicList,"logicalUnits[$n].actionName";
			push @logicList,"TERMINATION";
		}
		
		#通过to end拆分字符串
		my ($logic_from, $logci_to) = split /to|end/,$oneLogic;
		if($logci_to){
			push @logicList,"logicalUnits[$n].result.qid";
			#这里取出q后的字符串作为questid_arrRef的下标 然后返回qid
			push @logicList,$questid_arrRef->[substr($logci_to,1)-1];	
		}
		
		#通过$logic_from确定and or的数量并塞入数组	1or 2and
		my ($count_and, $count_or);
		$count_and++ while($logic_from =~ /and/ig);
		$count_or++ while($logic_from =~ /or/ig);
		for(1..$count_and){
			push @logicList,"logicalUnits[$n].relCode";
			push @logicList,"2";
		}
		for(1..$count_or){
			push @logicList,"logicalUnits[$n].relCode";
			push @logicList,"1";
		}
		
		#通过and or细分逻辑元素,并且循环逻辑元素并组装数据
		my @logic_element = split /and|or/,$logic_from;
		for(my $m=0; $m<=$#logic_element; $m++){
			$logic_element[$m] =~ s/(^\s+|\s+$)//g;
			my ($qid, $selected, $optIds) = split /-/,$logic_element[$m];
			#预处理细分元素
			$qid = $questid_arrRef->[substr($qid,1)-1];
			$selected = 1 if($selected eq "select");
			$selected = 0 if($selected eq "noselect");
			#组装数据
			push @logicList,"logicalUnits[$n].conditions[$m].qid";
			push @logicList,$qid;
			push @logicList,"logicalUnits[$n].conditions[$m].selected";
			push @logicList,$selected;
			push @logicList,"logicalUnits[$n].conditions[$m].optIds";
			push @logicList,$optIds;
		}

	}
	
#	for(my $i=0; $i<$#logicList; $i+=2){
#		my $nextI = $i + 1; 
#		myprint("encode", "$logicList[$i] -- $logicList[$nextI]\n");
#	}
	
	print "Start to create logical\n";
	my $return = $openbrowser->post("$open_addr/survey/a/editLogical",{@logicList},
		'Content-Type'=>"application/x-www-form-urlencoded;",
	)->content;
	&verification("mgt","editLogical",$openbrowser,$return);

	
}
1;