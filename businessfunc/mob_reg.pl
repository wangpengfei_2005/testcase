#手机端注册
use strict;

sub mob_reg
{
	my ($browser,$survey,$mob_addr,$account,$updateInfo) = @_;
	my @survey = @$survey;
	my %updateInfo = %$updateInfo;
	#my $password = 'MTIzNDU2';			#123456
	my $password = 'MTExMTExMTE=';		#'MTExMTExMTE='->11111111  'MTIzNDU2'->123456
	my $dbh = &connection(@survey);		#获取操作数据库的对象
	


	#请求头
	my %head = (
		'Content-Type'=>"application/x-www-form-urlencoded",
		'User-Agent'=>"Mozilla/5.0",
		'Accept-Language'=>"zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3",
		'Accept'=>"application/json;charset=UTF-8",
	);
	
	#解析$updateInfo中的用户属性(预处理)
	my (%regestinfo,%updateuser);	#前者用于提交身份证 后者用户提交其它属性
	#获取%regestinfo提交身份证的数据
	my @idList = ();
	my $idcard = "";			#用户一旦出现重复身份证提交就零时创建一个身份证之用
	$regestinfo{'birthday'} = $updateInfo{'birthday'}	if($updateInfo{'birthday'});
	$regestinfo{'sex'} = $updateInfo{'sex'} 			if($updateInfo{'sex'});
	#如果需要为虚拟用户生成身份证号列表
	#提交身份证属性 提交方法名/usersvc/regestifo
	if($updateInfo{'birthday'} || $updateInfo{'sex'}) {
		#调用身份证生成函数
		@idList = &create_ID("", $updateInfo{'birthday'} , $updateInfo{'sex'} , 1 );	
	}
	my $idError = 0;	#用于判断可能身份证提交出现异常
	
	#获取%updateuser提交其它属性的数据
	$updateuser{'education'} = $updateInfo{'education'}			if($updateInfo{'education'});
	$updateuser{'income'} = $updateInfo{'income'}				if($updateInfo{'income'});
	$updateuser{'incomeFamily'} = $updateInfo{'incomeFamily'}	if($updateInfo{'incomeFamily'});
	$updateuser{'marriage'} = $updateInfo{'marriage'}			if($updateInfo{'marriage'});
	$updateuser{'position'} = $updateInfo{'position'}			if($updateInfo{'position'});
	$updateuser{'profession'} = $updateInfo{'profession'}		if($updateInfo{'profession'});
	$updateuser{'vocation'} = $updateInfo{'vocation'}			if($updateInfo{'vocation'});
	
	
	# 开始注册15509860001
	for(1..1){
		my $i = 0;
		print "用户 $account,开始注册..";
		
		# 第一次注册获取验证码
		my $return = $browser->post("$mob_addr/survey-ws/usersvc/registerCheck",
			{
				'cellphone'=>$account,
				'password'=>$password,	
				'handle'=>$account,
			},
			%head
		)->content;
		my $result = &verification("mob","mob_reg",$browser,$return);
		#处理重命名
		if($result eq 3){
			my @mobFirsthree = qw{134 135 136 137 138 139 150 151 152 157 158 159 130 131 132 155 156};
			my $account_Prefix_2 = sprintf("%08d", &random(1,99999999));
			my $account_Prefix = $mobFirsthree[&random(0,$#mobFirsthree)] . $account_Prefix_2;
			print "-redo1-oldUsername:$account  ";
			$account = $account_Prefix;
			print "newUsername:$account\n";
			redo;
		}
		#异常处理断线后重连并重新注册
		unless ($result){
			$dbh = &connection(@survey);
			my $ck=HTTP::Cookies->new();
			$browser=LWP::UserAgent->new(agent=>'Mozilla/5.0',cookie_jar=>$ck);
			sleep 1;
			print "-redo2-";
			redo;
		}


		#查询数据库返回验证码
		my $sql = "SELECT `CODE` FROM `SMS_CODE` WHERE CELLPHONE='$account';";
		while(!$dbh){
			$dbh = &connection(@survey);
		}
		my $smscode = &operate_mysql_new($dbh, $sql, "select");

		#第二次注册请求
		my $return = $browser->post("$mob_addr/survey-ws/usersvc/register",
			{
				'cellphone'=>$account,
				'password'=>$password,
				'nickname'=>$account,
				'code'=>$smscode,
				'devicetype'=>'2',
				'devicetoken'=>'2',
			},
			%head
		)->content;
		
		#异常处理，返回失败后清空相关数据
		unless (&verification("mob","mob_reg_step2",$browser,$return)){
			&deleteAccount($account,\@survey);
			print "-redo3-";
			redo;
		}

		#开始配置用户数据,单次循环只能配置同样属性的用户	
		#当$updateInfo有配置时才执行
		if ($updateInfo){
			print "配置用户数据..";
			my ($mobrowser,$accessToken) = &moblogin($mob_addr,$account,'MTExMTExMTE=');
			my %login_head = (
				'Authorization'=> "Bearer $accessToken",
				'Accept' => "application/json;charset=UTF-8",
			);
			my $ver_return = 1;	#接受验证结果,如果异常就重新注册

			
			#解析%updateuser中的用户属性(再处理用于提交表单) 
			#%regestinfo,中的数据为身份证数据,在身份证函数中已经随机操作 不做处理
			#int(a+rand (b-a))
			my %do_updateuser = %updateuser;
			for my $key (keys %do_updateuser){
				my @temp = ();
				#删除空值的元素
				delete $do_updateuser{$key} unless($do_updateuser{$key});
				if($do_updateuser{$key} =~ /-/){	#解析一个范围值并给出其中的随机数
					@temp = split /-/,$do_updateuser{$key};
					$do_updateuser{$key} = int($temp[0]+rand ($temp[1]-$temp[0]+1));
				}
				if($do_updateuser{$key} =~ /,/){	#解析一个列标志并给出其中的随机选择
					@temp = split /,/,$do_updateuser{$key};
					$do_updateuser{$key} = $temp[int(0+rand $#temp+1)];
				}
			}		

			
			#提交身份证属性 提交方法名/usersvc/regestifo
			#@idList
			if($updateInfo{'birthday'} || $updateInfo{'sex'}){
				my $id;
				if($idError){
					#如果出错了 就在创建一个身份证
					$id = (&create_ID("","","",""))[0];
				}else{
					#没用错就用预置的列表
					$id = $idList[$i];
				}
				#测试,显示用户的身份证
				print " id:$id ";
				my $return = $browser->post("$mob_addr/survey-ws/usersvc/regestInfo",{
					'username' => 'testUser',
					'idcard' => $id,
					#'idcard' => '310103198504096039',
					},%login_head)->content;
				$ver_return = &verification("mob","updateuser",$browser,$return);

				unless($ver_return){
					&deleteAccount($account,\@survey);
					$idError = 1;
					redo;
				}else{
					$idError = 0;
				}
				
			}
			
			
			
			#提交其它属性 提交方法名/usersvc/updateuser
			#测试,打印出该用户的属性
			for my $k(keys %do_updateuser){
				my %h = &quota_index($k);
				print "\($h{$do_updateuser{$k}}\) ";
			}
			my $return = $browser->post("$mob_addr/survey-ws/usersvc/updateuser",{%do_updateuser},%login_head)->content;
			$ver_return = &verification("mob","updateuser",$browser,$return);
			unless($ver_return){
				&deleteAccount($account,\@survey);
				redo;
			}

		}
		
		sleep 1;
		print "注册完毕\n\n";
	}
	
	return 1;

	
	sub deleteAccount
	{
		my ($account,$survey) = @_;
		my @survey = @$survey;
		my @sql = (	"DELETE FROM USER_ACCOUNT WHERE LOGIN_NAME='$account';",
					"DELETE FROM USER_PROFILE WHERE NICK_NAME='$account';",
					"DELETE FROM SMS_CODE WHERE CELLPHONE='$account';"
		);
		$dbh = &connection(@survey);
		for(@sql){
			&operate_mysql_new($dbh, $_, "insert");
		}
		sleep 1;
		return 1;
	}
	
	
}

1;