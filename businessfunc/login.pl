#函数功能为登录，管理后台和手机客户端
use strict;


sub moblogin
{
	#登录请求
	my ($mob_addr,$username,$password) = @_;

	####
	#手机端请求
	#初始化http协议
	####
	my $ck=HTTP::Cookies->new();
	my $browser=LWP::UserAgent->new(agent=>'Mozilla/5.0',cookie_jar=>$ck);
	
	#有传密码就使用默认密码,如果登录出错尝试使用默认的密码,如果默认密码也无法登录提示出错
	#'MTExMTExMTE=' => '11111111'
	#'MTIzNDU2' => 123456
	my @defPasswd = ('MTExMTExMTE=','MTIzNDU2');
	#判断$password是否有值并插入defPasswd前端
	unshift @defPasswd,$password if($password);
	
	my %head = (
		'Content-Type'=>"application/x-www-form-urlencoded",
		'User-Agent'=>"sMozilla/5.0",
		'Accept-Language'=>"zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3",
		'Accept'=>"application/json;charset=UTF-8"
	);
	my $login;
	
	my $passwdindex = 0;
	while(1){	#如果默认的密码用完,就提示无法登录
		#print "--debug-- $passwdindex  $defPasswd[$passwdindex]\n";
		$login = $browser->post("$mob_addr/survey-ws/usersvc/signIn",
			{
			username=>$username,
			password=>$defPasswd[$passwdindex],
			source=>'zhuanlingyong',
			devicetype=>'2',
			devicetoken=>'',
			},
			%head,
		)->content;
		#print "do_login - $login\n";
		my $ret = &verification("mob","moblogin",$browser,$login);
		if($ret eq 4){
			print "$username Login return no a json,relogin\n";
			return (0,0);
		}
		if($ret eq 1){
			last;
		}
		if($ret eq 0){
			if($passwdindex == $#defPasswd){
				print "--error.login-- User passwd and default passwd all used, but $username still can't login!! Please check you password\n";
				return (0,0);
			}
			$passwdindex++;
		}
		print LOG "do_login - $login\n";
	}
	
	#获取token值
	my $token;
	if($login =~ /"accessToken":"(.*?)"/){
		return ($browser,$1);
	}
}

#管理后台登录请求
sub mgtlogin
{
	#登录请求
	my ($mgt_addr,$username) = @_;
	
	####
	#管理后台发送消息
	#初始化http协议
	####
	my $ck1=HTTP::Cookies->new();
	my $browser1=LWP::UserAgent->new(agent=>'Mozilla/5.0',cookie_jar=>$ck1);
	my $login = $browser1->post("$mgt_addr/login",
		{
			#username=>'admin',
			#password=>'@xinchunge',
			username=>'admin',
			password=>'11111111',
		},

	)->content;
	myprint("encode",$login);
	#(subname,returninfo)
	&verification("mgt","mgtlogin",$browser1,$login);
	
	return ($browser1);
}



#开放平台登录请求
sub openlogin
{
	#登录请求
	my ($open_addr,$username) = @_;
	
	####
	#管理后台发送消息
	#初始化http协议
	####
	my $ck1=HTTP::Cookies->new();
	my $browser1=LWP::UserAgent->new(agent=>'Mozilla/5.0',cookie_jar=>$ck1);
	my $login = $browser1->post("$open_addr/do_login",
		{
			username => $username,
			password => 'MTExMTExMTE=',
		},
		'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
	)->content;
	#myprint("encode",$login);
	#(subname,returninfo)
	&verification("mgt","openlogin",$browser1,$login);
	
	return ($browser1);
}


#管理后台登录请求https
#sub mgtloginForHttps
#{
#	my ($mgt_addr,$username) = @_;
#	####
#	#管理后台发送消息
#	#初始化http协议
#	####
#	my $ck1=HTTP::Cookies->new(
#		file => "lwp_cookies.dat",
#		autosave => 1,
#	);
#	my $browser1=LWP::UserAgent->new(
#		#ssl_opts => { verify_hostname => 1 },
#		agent=>'Mozilla/5.0',
#		cookie_jar=>$ck1
#	);
#	#https://10.201.2.12:8443/login		#$mgt_addr/login
#	my $login = $browser1->post("$mgt_addr/login",
#		{
#		username=>'admin',
#		password=>'11111111',
#		},
#	)->content;
#	#(subname,returninfo)
#	
#	print "--debug-- $login\n";
#	&verification("mgt","mgtlogin",$browser1,$login);
#	
#	return ($browser1);
#}




1;