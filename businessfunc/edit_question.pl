#创建题目
use strict;
####
#问卷题目编辑函数
#外部传进来问题的选项数组$hash_q，test,pic,test,pic 一一对应
####
sub edit_question
{
	my ($mgtbrowser,
		$mgt_addr,
		$type,
		$surveyid,
		$surtednum,
		$questitle,
		$titlepic,
		$hash_q,
		$flagReference,
		$refSource,
		$refquest,
		$duration) = @_;
	my @hash_quest = @$hash_q;
	my @referenceSource = @$refSource unless($refSource eq "");
	my @refquestno = @$refquest unless($refquest eq "");
	my %questionType = (
		'1' => "单选题",
		'2' => "多选题",
		'3' => "尺度题",
		'4' => "数字题",
		'5' => "文字题",
		'7' => "视频题",
		'r' => "引用题",
		'8' => "语音题",
	);
	######
	#处理数据
	######
	#显示出创建的题目序号和类型
	my $qt = $questionType{$type};
	$qt = $questionType{'r'} if($flagReference);
	print "创建第 $surtednum 题 , 问卷类型:$qt \n";
	
	#哈希模板
	#通过传入选项数组，完成哈希键值组装
	my %hash_model = {};
	#组装第一次请求的optionstext参数,用选项的描述组装
	my ($optionstext,@optionstext_temp);
	#类型1 2单选和多选
	if($type eq 1 || $type eq 2){
		my $inno = 1;
		my $n=0;
		for (@hash_quest){
			if($inno == 1){
				$hash_model{"options[$n].text"} = $_ ;
				push @optionstext_temp,$_;
			}
			if($inno == 2){
				#print "--debug picname: $_ \n";
				$hash_model{"optionFile$n"} = ["$_"];
				$hash_model{"options[$n].imageUrl"} = "";
				$inno=1,$n++,next;
			}
			$inno++;
		}
		$optionstext = join "%0A",@optionstext_temp;
	}
	#类型为3，尺度题
	if($type eq 3){
		$hash_model{"scaleNum"} = $hash_quest[0];
		$hash_model{"leftDesc"} = $hash_quest[1];
		$hash_model{"rightDesc"} = $hash_quest[2];
	}
	#类型为7，视频题
	if($type eq 7){
		$hash_model{"questionVideoFile"} = ["$hash_quest[0]"];
		$hash_model{"skipFlag"} = $hash_quest[1];
		$hash_model{"pauseFlag"} = $hash_quest[2];
	}
	#类型为8，视频题
	if($type eq 8){

	}
	#调试
	#while(my($k,$v) = each %hash_model){
	#	print "debug---$k,$v\n";
	#}
	
	
	#####
	#开始网络请求
	#####
	my $return_json;
	my $id;
	#http://10.201.70.81/survey/a/addOptionsText?surveyId=2932&optionsText=1111%0A2222%0A3333&questionType=1&content=danxuan&sortedNum=1&questionId=
	#管理后台生成题目
	if(($type eq 1 && $flagReference eq 0) || ($type eq 2)){	#类型7是视频,没有这一步操作
		my $addOptionsText = "$mgt_addr/survey/a/addOptionsText?surveyId=$surveyid";
		$addOptionsText =  $addOptionsText . "";
	
		$return_json = $mgtbrowser->get(
		"$mgt_addr/survey/a/addOptionsText?surveyId=$surveyid&optionsText=$optionstext&questionType=$type&content=$questitle&sortedNum=$surtednum&questionId="
		)->content;
		&verification("mgt","edit_question.addOptionsText",$mgtbrowser,$return_json);
		if($return_json =~ /"id":"(\d+)"/){
			$id = $1;
		}
	}
	
	my %addQuestion_hash;
	my @addQuestion_arr;
	#不同的条件改变哈希模板的键值(1～5的类型组装的post参数)	
	if($type>=1 && $type<=5){
		%addQuestion_hash = (
			surveyId=>$surveyid,
			content=>$surtednum.". ".$questitle,
			type=>$type,
			sortedNum=>$surtednum,
			id=>$id,
			questionFile=>["$titlepic"],
			questionImage=>"",
			questionDesc=>"",
			questionDescFile=>"",
			questionDescPic=>"",
			optionsText=>$optionstext,
	
			%hash_model
		);
		#问题描述的选项

	}
	#不同的条件改变哈希模板的键值(7的类型组装的post参数)	
	if($type eq 7){
		%addQuestion_hash = (
			surveyId=>$surveyid,
			content=>$surtednum.". ".$questitle,
			type=>$type,
			sortedNum=>$surtednum,
			id=>$id,
			questionDesc=>"",
			questionDescFile=>"",
			questionDescPic=>"",
			videoURL=>"",
			questionRearDesc=>"",
			playNum=>"",
			%hash_model
		);
	}
	
	#不同的条件改变哈希模板的键值(7的类型组装的post参数)	
	if($type eq 8){
		%addQuestion_hash = (
			surveyId=>$surveyid,
			content=>$surtednum.". ".$questitle,
			type=>"$type",
			sortedNum=>$surtednum,
			id=>"",
			questionFile=>'',
			questionImage=>'',
			questionDesc=>'',
			questionDescFile=>'',
			questionDescPic=>'',
			duration=>$duration,
		);
	}
	
	
	#类型1和2的又是引用题的
	if(($type eq 1 || $type eq 2) && $flagReference eq 1){
		#组装引用题的题源
		my @refquestarr;
		for(@refquestno){
			push @refquestarr,'referenceSource';
			push @refquestarr,$referenceSource[$_-1];
		}
		#组装引用题post报文
		@addQuestion_arr = (
			'surveyId' , $surveyid,
			'type' , $type,
			'content' , $surtednum.". ".$questitle,
			'sortedNum' , $surtednum,
			'id' , $id,
			'flagReference' , $flagReference,
			'questionFile' , "",
			'questionImage' , "",
			'questionDesc' , "",
			'questionDescFile' , "",
			'questionDescPic' , "",
			'maxOptional' , "5",
			@refquestarr,
			
		);
		
		#最终生成题目(点击那个继续按钮)
		#http://10.201.70.81/survey/a/addQuestion
		#http://10.201.70.81/survey/questions/create
		$return_json = $mgtbrowser->post("$mgt_addr/survey/questions/create",
			[@addQuestion_arr],
			'Referer'=>"$mgt_addr/survey/questions/show/$surveyid",
			'Content-Type'=>"multipart/form-data;",
		)->content;
		&verification("mgt","edit_question.addQuestion",$mgtbrowser,$return_json);

	}else{
		#最终生成题目(点击那个继续按钮)
		#http://10.201.70.81/survey/a/addQuestion
		$return_json = $mgtbrowser->post("$mgt_addr/survey/questions/create",
			{%addQuestion_hash},
			'Referer'=>"$mgt_addr/survey/questions/show/$surveyid",
			'Content-Type'=>"multipart/form-data;",
		)->content;
		&verification("mgt","edit_question.addQuestion",$mgtbrowser,$return_json);
	}
	

	
	

	my $qustionid;
	if($return_json =~ /"id":"(\d+)"/){
		$qustionid = $1;
		#print "-------$1\n";
	}
	#print "addOptionsText2 - $return_json\n";
	#print LOG "addOptionsText2 - $return_json\n";
	
	return $qustionid;
}
1;