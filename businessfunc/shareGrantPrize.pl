﻿#模拟用户开始答题

use strict;

sub shareGrantPrize
{
	my ($mob_addr, $surveyStr, $mobrowser, $accessToken, $sunnyId, $sharedContent) = @_;

	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	
	
	#开始分享
	my $returnJson_3 = $mobrowser->post(
		"$mob_addr/survey-ws/surveySunny/shareGrantPrize",
		{
			sunnyId => $sunnyId,
			sharedContent => $sharedContent,
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	unless (&verification("mob","shareGrantPrize",$mobrowser,$returnJson_3)){
		return 0;
	}
	

	return 1;
}
1;