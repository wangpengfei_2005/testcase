#最终生成题目
use strict;

sub create
{
	my ($mgtbrowser,$mgt_addr,$post) = @_;
	#####
	#开始网络请求 点击生成
	#####
	my ($return_json,$questionId);

	$return_json = $mgtbrowser->post("$mgt_addr/survey/questions/create",
			[@$post],
			#'Referer'=>"$mgt_addr/survey/questions/show/$surveyid",
			'Content-Type'=>"multipart/form-data;",
	)->content;
	&verification("mgt","edit_question.create",$mgtbrowser,$return_json);
	if($return_json =~ /"id":"(\d+)"/){
		$questionId = $1;
	}
	
	return $questionId;
}
1;