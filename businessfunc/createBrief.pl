#最终生成题目
use strict;

sub createBrief
{
	my ($mob_addr,$mobrowser,$accessToken,$createBriefPost) = @_;
	#####
	#开始网络请求 点击生成
	#####
	my ($return_json,$surveyId);

	$return_json = $mobrowser->post("$mob_addr/survey-ws/vi/surveysvc/createBrief",
			{@{$createBriefPost}},
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",
	)->content;
	&verification("mob","createBrief",$mobrowser,$return_json);
	
	if($return_json =~ /"surveyId":(\d+)/){
		$surveyId = $1;
	}
	return $surveyId;
}
1;