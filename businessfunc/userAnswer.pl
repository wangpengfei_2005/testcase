﻿#模拟用户开始答题

use strict;

sub userAnswer
{
	my ($mob_addr,$surveyStr,$useraccount,$surveyid,$answerList) = @_;
	my @survey = @$surveyStr;
	my %answerList = %$answerList;
	
	#题目类型哈希
	my %questType = (
		"1" => "单选题",
		"2" => "多选题",
		"3" => "尺度题",
		"4" => "文本题-数字",
		"5" => "文本题-文字",
		"6" => "图片题",
		"7" => "视频题",
		"8" => "语音题",
		"9" => "定位题",
		"10" => "web页面题",
	);
	
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	#(获取选项id(sortedNum), 获取题目类型, 获取选项总量, 获取是否终结的值)
	my ($answerSrt,$sortedNum,$questionType,$optionCount,$allowWord,$isEnd,$sunnySwitch); 
	$isEnd = "false";
	#返回手机登录后的会话
	my ($mobrowser,$accessToken);
	
	#手机端的登录请求
	my @singleUser = ();	#保存单个用户的信息
	my $userString;			#组装用户信息后插入所有用户数组中（;号隔开元素  ,号隔开选项之间  :隔开题号与选项 -隔开多选题答案）
	chomp($useraccount);
	push @singleUser,$useraccount;	#第一个元素用户名
	&myprint("encode","$useraccount 用户登录..");

	#手机登录请求
	#'MTExMTExMTE=' => '11111111'
	#'MTIzNDU2' => 123456
	($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
	my $errCount = 0;
	while($mobrowser eq 0){	#登录失败尝试重新登录
		($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
		if($errCount++ eq 5){
			print "login err counts equal 5 and give up\n";
			return 0;
		}
		
	}
	

	
	#开始秒杀请求
	&myprint("encode","秒杀..");
	my $seckilRec = 0;	#记录开始答题请求错误次数
	my $ReturnJson = &seckill($mobrowser,$accessToken,$surveyid);
	while($ReturnJson eq 0){
		&myprint("encode","秒杀出现异常尝试重新请求\n");
		$ReturnJson = &seckill($mobrowser,$accessToken,$surveyid);
	}
	#unless($ReturnJson){
	#	&myprint("encode","答题结束(无法秒杀)\n");
	#	push @singleUser,"seckillError";
	#	return (join ";",@singleUser);
	#}
	
	#处理超时用户
	if($answerList{"timeout"}){		#超时
		&myprint("encode","超时用户..答题结束\n");
		push @singleUser,"timeout";
		return (join ";",@singleUser);
	}
	
	#处理返回的数据
	$json_obj = $json->decode("$ReturnJson");
	#获取选项id(sortedNum)
	$sortedNum = $json_obj->{'data'}->{'questionDetail'}->{'sortedNum'};
	#获取题目类型
	$questionType = $json_obj->{'data'}->{'questionDetail'}->{'type'};
	#获取选项总量
	$optionCount = $json_obj->{'data'}->{'questionDetail'}->{'optionCount'};
	#是否有其它选项
	$allowWord = $json_obj->{'data'}->{'questionDetail'}->{'allowWord'};
	#获取是否答题结束的值
	
	
	
	&myprint("encode","开始答题..");
	#开始循环答题		
	while($isEnd eq "false"){

		#解析答案
		#从外面传进来的答案hash获得该题的答案
		#如果为空就默认做随机答题操作
		my $thisAnswer = "";
		$thisAnswer = $answerList{$sortedNum};
		#print "*** thisAnswer - $thisAnswer ***";
		#$| =1 ;
		
		#解析预置答案,否则就随机回答问题
		#min,max存放范围值以便随机其中的值
		#@T_thisAnswerList,@thisAnswerList 前者是临时数组,后者存放可用的答案
		#@AnswerStr存放字符串答案，[0]为多选时的序号,[1]则是字符串
		my ($min,$max,@T_thisAnswerList,@thisAnswerList,@AnswerStr);
		if($thisAnswer){		
			@T_thisAnswerList = split /,/,$thisAnswer;
			for my $e (@T_thisAnswerList){
				if($e =~ /-/){
					($min,$max) = split /-/,$e;
					for my $le ($min..$max){
						push @thisAnswerList,$le;
					}
					next;
				}
				if($e =~ /_/){
					@AnswerStr = split /_/,$e;
					next;
				}
				push @thisAnswerList,$e;
			}
		}else{
			#这里处理随机答案
			$thisAnswer = "random";
		}
		
		#测试
		#print "** $_ ***" for(@thisAnswerList);
		
		#初始化存放answserStep接口的报文参数
		my %answerStepGetInfo = ();
		my $optionid;
		#先组装一些不变的参数
		$answerStepGetInfo{"questionid"} = $sortedNum;
		$answerStepGetInfo{"stepType"} = "2";
		
		#不同的类型组装答案 存入%answerStepGetInf中
		my $myanswer;
		&myprint("encode",$questType{$questionType});
		
		#单选题
		if($questionType eq "1"){	
			if($thisAnswer eq "random"){	#完全随机时的处理
				if($allowWord eq "true"){	#处理有其它选项时
					$optionCount++;
					$myanswer = &random(1,$optionCount);
					if($myanswer eq $optionCount){
						$answerStepGetInfo{"answer"} = "The Random String";
					}else{
						$answerStepGetInfo{"optionid"} = $myanswer;
					}
				}else{
					$myanswer = &random(1,$optionCount);
					$answerStepGetInfo{"optionid"} = $myanswer;
				}
			}else{		#这里是指定答案时的处理
				if($AnswerStr[1]){
					$answerStepGetInfo{"answer"} = $AnswerStr[1];
				}else{
					$answerStepGetInfo{"optionid"} = $thisAnswerList[&random(0,$#thisAnswerList)];
				}
			}
		}
		
		#多选
		if($questionType eq "2"){	
			#这里需要解释出json中的maxOptional最大可选数量
			my $maxOptional = ($json_obj->{'data'}->{'questionDetail'}->{'optionScheme'})[0]->{'maxOptional'};
			if($thisAnswer eq "random"){	#完全随机时的处理
				my $maxchoose = &random(1,$maxOptional);	#获取最大可选数量的随机值
				my %answerSt = ();
				if($allowWord eq "true"){	#当处理有其它选项时
					#$optionCount++;
					while($maxchoose > keys %answerSt){
						$answerSt{&random(1,$optionCount)} = 1;
					}
					if($answerSt{$optionCount}){
						delete $answerSt{$optionCount};
						$answerStepGetInfo{"answer"} = "The Random String";
					}
					$answerStepGetInfo{"optionid"} = join ",",(keys %answerSt);
				}else{				#没有其它选项的
					while($maxchoose > keys %answerSt){
						$answerSt{&random(1,$optionCount)} = 1;
					}
					$answerStepGetInfo{"optionid"} = join ",",(keys %answerSt);
				}
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"answer"} = $AnswerStr[1];
				$answerStepGetInfo{"optionid"} = join ",",@thisAnswerList;
			}
		}
		
		#尺度
		if($questionType eq "3"){	
			my $ruleStart = 1;
			my $ruleEnd = $optionCount;
			if($thisAnswer eq "random"){	#完全随机时的处理
				$myanswer = &random($ruleStart,$ruleEnd);
				$answerStepGetInfo{"answer"} = $myanswer;
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"answer"} = $thisAnswerList[&random(0,$#thisAnswerList)];
			}
		}
		
		#文本题-数字
		if($questionType eq "4"){	
			if($thisAnswer eq "random"){	#完全随机时的处理
				$myanswer = &random(198500,198549);
				$answerStepGetInfo{"answer"} = $myanswer;
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"answer"} = $AnswerStr[1];
			}
		}
		
		#文字题-文字
		if($questionType eq "5"){	
			if($thisAnswer eq "random"){	#完全随机时的处理
				my @myanswer_5 = ("I love you","I hate you","I like you","I fuck you");
				$myanswer = $myanswer_5[&random(0,$#myanswer_5)];
				$answerStepGetInfo{"answer"} = $myanswer;
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"answer"} = $thisAnswer;
			}
		}
		
		#图片题
		if($questionType eq "6"){	
			#图片题的被选图片
			my @pictures = ("http://qa-mgt-image.u.qiniudn.com/Kb7NjB7EwL_1405662342845.jpg",
							"http://qa-mgt-image.u.qiniudn.com/TbTR2F3jsd_1405662343171.jpg",
							"http://qa-mgt-image.u.qiniudn.com/hE6u1RAYHe_1405662343545.jpg"
			);
			if($thisAnswer eq "random"){	#完全随机时的处理
				$answerStepGetInfo{"pictureUploadUrl"} = $pictures[&random(0,$#pictures)];
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"pictureUploadUrl"} = $thisAnswer;
			}
		}
		
		#视频题
		if($questionType eq "7"){	
			#视频题什么都不传....
		}
		
		#语音题
		if($questionType eq "8"){
			#语音题的被选地址
			my %voice = (	
				'http://qa-video.u.qiniudn.com/FvxhFjuqEUc-K-Uzx5F1dV8c-geN' => '9',
				'http://qa-video.u.qiniudn.com/iOS_Audio_2592_1_20140722150822886_845F2CF6E8BA477592A6BE5267CCB6D7.mp3' => '10',
				'http://qa-video.u.qiniudn.com/iOS_Audio_2594_1_20140715155122335_D9444F8EE6B94E6387EB1885245B098B.mp3' => '11'
			);
			if($thisAnswer eq "random"){	#完全随机时的处理
				my @voiceAddresses = keys %voice;				
				my $randAdd = &random(0, $#voiceAddresses);
				$answerStepGetInfo{"pictureUploadUrl"} = $voiceAddresses[$randAdd];
				$answerStepGetInfo{"duration"} = $voice{$voiceAddresses[$randAdd]};
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"duration"} = $AnswerStr[1];
				$answerStepGetInfo{"pictureUploadUrl"} = $AnswerStr[0];
			}
		}
		
		#定位题
		if($questionType eq "9"){	
			my @loction = (
				"IOS,87.64,-12.33,上海市黄浦区44号",
				"IOS,23.54,-84.64,上海市徐汇区天钥桥路1号",
				"android,24.57,-34.54,上海市卢湾区丽园路994弄",
				"android,75.34,-53.12,上海市浦东新区联航路1999号"
			);
			if($thisAnswer eq "random"){	#完全随机时的处理
				$answerStepGetInfo{"answer"} = $loction[&random(0,$#loction)];
			}else{		#这里是指定答案时的处理
				$answerStepGetInfo{"pictureUploadUrl"} = $thisAnswer;
			}
		}
		
		#web跳转题
		if($questionType eq "10"){
			#还是什么都不用做
			#并且有可能以后需要读取第三方的页面并尝试提交一些答题信息
			
		}
		
		#报文组装完毕,请求答题接口
		$ReturnJson = &answerStep($mobrowser,$accessToken,$surveyid,\%answerStepGetInfo);
		push (@singleUser,$answerSrt);
		my $answerStepRec = 0;	#记录答题过程请求错误次数
		while($ReturnJson eq 0){
			&myprint("encode","答题出现异常尝试重新请求\n");
			$ReturnJson = &answerStep($mobrowser,$accessToken,$surveyid,\%answerStepGetInfo);
		}
		#unless($ReturnJson){	#就收到异常后退出并继续下一个用户的答题
		#	&myprint("encode","答题结束(答题异常)\n");
		#	push @singleUser,"answerSrtError";
		#	return (join ";",@singleUser);
		#	return 0;
		#}
		
		#处理返回的数据
		$json_obj = $json->decode("$ReturnJson");
		#获取选项id(sortedNum)
		$sortedNum = $json_obj->{'data'}->{'questionDetail'}->{'sortedNum'};
		#获取题目类型
		$questionType = $json_obj->{'data'}->{'questionDetail'}->{'type'};
		#获取选项总量
		$optionCount = $json_obj->{'data'}->{'questionDetail'}->{'optionCount'};
		#是否有其它选项
		$allowWord = $json_obj->{'data'}->{'questionDetail'}->{'allowWord'};
		#获取是否答题结束的值
		$isEnd = $json_obj->{'data'}->{'isEnd'};
		#答题后抽奖标识
		$sunnySwitch = $json_obj->{'data'}->{'sunnySwitch'};
		
		print " sunnySwitch = $sunnySwitch "
	}
	

	&myprint("encode","答题结束\n\n");
	return (join ";",@singleUser);
	
	
	
	
	sub seckill
	{
		my ($mobrowser,$accessToken,$surveyid) = @_;
		my $sortedNum;
		#开始秒杀请求
		#print "debug---seckill  surveyid=$surveyid  sortedNum=$sortedNum\n";
		my $returnJson = $mobrowser->post("$mob_addr/survey-ws/surveysvc/seckill",
			{
				'surveyid' => $surveyid,
				'addressid' => "11",
				'version' => '2.0.0'
			},
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",
		)->content;
		#&verification("mob","survey_answer.seckill",$mobrowser,$return);
		#&myprint("encode","seckill ---  $returnJson \n");
		unless (&verification("mob","survey_answer.seckill",$mobrowser,$returnJson)){
			return 0;
		}
		
		return $returnJson;
		
	}

	
	sub answerStep
	{
		#外部输入强制虚拟用户选择指定选项
		#$optionid强制选择的id, $unselectid强制不选择的id
		my ($mobrowser,$accessToken,$surveyid,$answerStepGetInfo) = @_;
		my %answerStepGetInfo = %$answerStepGetInfo;
		#题目类型哈希
		
		#开始答题
		&myprint("encode","(qid:".$answerStepGetInfo{'questionid'}." "."oid:".$answerStepGetInfo{'optionid'}." "."other:".$answerStepGetInfo{'answer'}.") ");
		#$|=1;	#清除缓存以便立即打印结果在终端
		
		#组装get参数
		my $getStr = "$mob_addr/survey-ws/surveysvc/answerStep?surveyid=$surveyid&stepType=2";
		$getStr = $getStr."&"."questionid=".$answerStepGetInfo{"questionid"} if($answerStepGetInfo{"questionid"});
		$getStr = $getStr."&"."answer=".$answerStepGetInfo{"answer"} if($answerStepGetInfo{"answer"});
		$getStr = $getStr."&"."optionid=".$answerStepGetInfo{"optionid"} if($answerStepGetInfo{"optionid"});
		$getStr = $getStr."&"."pictureUploadUrl=".$answerStepGetInfo{"pictureUploadUrl"} if($answerStepGetInfo{"pictureUploadUrl"});
		$getStr = $getStr."&"."duration=".$answerStepGetInfo{"duration"} if($answerStepGetInfo{"duration"});
			
		
		#$getStr = decode("utf-8", $getStr);
		#测试
		#print "debug-answerStep----getStr:$getStr\n";
		
		#发送第一题的请求
		#&myprint("encode","answerStep ---  $getStr \n");
		my $returnJson = $mobrowser->get($getStr,
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",
		)->content;
		#&myprint("encode","debug-answerStep  ----  returnJson:$returnJson\n");
		
		my $ret = &verification("mob","survey_answer.answerStep",$mobrowser,$returnJson);
		if($ret eq 2 || $ret eq 4){
			return 0;	#返回后外面的$answerSrt接受异常值
		}
		
		return $returnJson;
	}
	
}
1;