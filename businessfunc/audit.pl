#审核
#http://10.201.2.12/survey/a/survey_pass
#http://10.201.70.81/survey/audit

sub audit
{	
	#针对普通问卷的审核
	my ($mgtbrowser,$mgt_addr,$surveyid,%auditconfig) = @_;
	#print "now time" . &nowtime("1","1","1","1","1","","");
	#print "debug--" . &calculate_date(&nowtime("1","1","1","1","1","",""),"minute","+","2");
	
	print "Audit survey\n";
	
	$return_json = $mgtbrowser->post("$mgt_addr/survey/audit",
		{
		expectedTimeStr=>&calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","+","9"),
		period=>"1",			#调查限时
		startTimeStr=>&calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","+","5"),
		suggestPeriod=>"1",	#答题耗时
		surveyId=>$surveyid,
		surveyType=>"1",			#1.首页  2.推送  3.推送用户组
		userGroupId	=> "",
		partakeNum => "1",
		%auditconfig
		},
		'Content-Type'=>"application/x-www-form-urlencoded;",
	)->content;
	&verification("mgt","survey_pass",$mgtbrowser,$return_json);

}

1;