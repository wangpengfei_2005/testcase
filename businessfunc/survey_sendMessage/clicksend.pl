#给全体用户发送消息,点击发送的操作
use strict;

sub clicksend
{
	#http://10.201.2.12/message/find_multi_message?p=1&title=&status=&messageType=5
	#用户组messageType=5  全部用户:空
	my ($mgtbrowser,$mgt_addr,$messagetype,$ms_tital) = @_;
	#点击创建群组消息，获得列表页面
	my $return_json = $mgtbrowser->get("$mgt_addr/message/find_multi_message?p=1&title=&status=&messageType=$messagetype",
		'Referer'=>"$mgt_addr/message/message_edit",
		'Content-Type'=>"application/x-www-form-urlencoded; charset=UTF-8",
	)->content;
	&verification("mgt","find_multi_message",$mgtbrowser,$return_json);
	
	#从列表页面抓取idstr
	my $idstr;
	#if($return_json =~ /<td>1_(.*?)<\/td>/){
	#虽然有多个发送按钮，但是只需要正则匹配到第一个并抓取对应的数据，也算是曲线完成
	if($return_json =~ /onclick="updateMessageStatus\((.*?),1\)/){
		$idstr = $1;
	}
	
	print "idstr: $idstr\n";
	#点击发送
	#http://10.201.70.81/message/update_message_status?idStr=69&status=1
	my $return_json = $mgtbrowser->get("$mgt_addr/message/update_message_status?idStr=$idstr&status=1",
		'Referer'=>"$mgt_addr/message/message_edit",
		'Content-Type'=>"application/x-www-form-urlencoded; charset=UTF-8",
	)->content;
	&verification("mgt","update_message_status",$mgtbrowser,$return_json);

	#return ;
	
}
1;