#手机收取消息并验证信息
use strict;

sub receive_msg
{	
	my ($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase) = @_;

	#手机收取消息并验证信息
	#每隔1秒请求一次,一共10次.
	my $return_json;
	for(my $i=0; $i<10; $i++){
		$return_json = $mobrowser->get("$mob_addr/survey-ws/msgsvc/msgs?page=0&pagesize=10",
			Authorization=>$accessToken,
			Accept=>"application/json;charset=UTF-8",)->content;
		#&myprint("encode", "msg - $return_json\n");
		&verification("mob","msgs",$mobrowser,$return_json);
		
		my $moreconut = 0; #判断是否有多于的数据
		while($return_json =~ /"title":"(.*?)"/g){
			my $resuit_title = $1;
			if($resuit_title eq $ms_tital){
				print "$testcase\nmassage test pass V! \nresuit_title:\t$resuit_title\nms_tital:\t$ms_tital\n";
				$moreconut++;
			}
		}
		unless($moreconut){
			sleep 1;
			next ;
		}
		if($moreconut >1){
			print "$testcase\nmassage test faile X! find more message ,$moreconut\n";
			return 0 ;
		}
		return 1 if($moreconut ==1);
		
		
		
	}
	
	#没有符合预期的结果
	print "$testcase\nmassage test faile X! 没有符合预期的结果\n";
	return 0 ;

}
1;