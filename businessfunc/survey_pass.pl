#审核
#http://10.201.2.12/survey/a/survey_pass
#http://10.201.70.81/survey/audit

sub survey_pass
{	
	#针对普通问卷的审核
	my ($mgtbrowser,$mgt_addr,$surveyid,%auditconfig) = @_;
	#print "now time" . &nowtime("1","1","1","1","1","","");
	#print "debug--" . &calculate_date(&nowtime("1","1","1","1","1","",""),"minute","+","2");
	
	print "审核上架_针对普通问卷的审核\n";
	
	$return_json = $mgtbrowser->post("$mgt_addr/survey/audit",
		{
		expectedTimeStr=>&calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","+","9"),
		period=>"10",			#调查限时
		startTimeStr=>&calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","+","5"),
		suggestPeriod=>"1",	#答题耗时
		surveyId=>$surveyid,
		surveyType=>"1",			#1.首页  2.推送  3.推送用户组
		userGroupId	=> "",
		
		%auditconfig
		},
		'Referer'=>"$mgt_addr/survey/survey_list",
		'Content-Type'=>"application/x-www-form-urlencoded;",
	)->content;
	&verification("mgt","survey_pass",$mgtbrowser,$return_json);
	#print "survey_pass - $return_json\n";
	#print LOG "survey_pass - $return_json\n";
}

1;