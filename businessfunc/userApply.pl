#模拟用户开始答题
use strict;


sub userApply
{
	#mode=1普通模式随机答题
	#mode=2场景设置为10%人超时，10%人逻辑终止，80%人提交
	my ($mob_addr,$surveyStr,$userlistStr,$surveyid,$mode) = @_;
	my @userlist = @$userlistStr;
	my @survey = @$surveyStr;
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	#验证结果
	my ($check); 
	#返回手机登录后的会话
	my ($mobrowser,$accessToken);
	
	#手机端的登录请求
	for (my $i=0; $i<=$#userlist; $i++){
		chomp($userlist[$i]);
		print "$userlist[$i] 用户登录..";
		#手机登录请求
		#'MTExMTExMTE=' => '11111111'
		#'MTIzNDU2' => 123456
		($mobrowser,$accessToken) = &moblogin($mob_addr,$userlist[$i],'MTExMTExMTE=');
		unless($mobrowser){
			print "$userlist[$i] 用户登录失败尝试重新登录..\n";
			redo;
		}
		
		#开始报名
		if($mode eq 1){	#普通模式
			print "报名..";
			#开始报名
			($check) = &apply($mobrowser,$accessToken,$surveyid);
			unless($check){
				print "报名结束(无法报名)\n";
				next;
			}
	
		}

		print "报名结束\n";
	}

	return 1;
	
	
	sub apply
	{
		my ($mobrowser,$accessToken,$surveyid,$sortedNum) = @_;
		#开始秒杀请求
		#print "debug---seckill  surveyid=$surveyid  sortedNum=$sortedNum\n";
		my $return = $mobrowser->post("$mob_addr/survey-ws/surveysvc/applySurvey",
			{
				'surveyId' => $surveyid,
			},
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",
		)->content;
		#&verification("mob","survey_answer.seckill",$mobrowser,$return);
		unless (&verification("mob","userApply.apply",$mobrowser,$return)){
			return 0;
		}
		return 1;
		
	}


	
}
1;