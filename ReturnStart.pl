use strict;
use LWP::UserAgent;
use HTTP::Cookies;
use JSON;
use Encode;
use DBI;
use threads;
use HTTP::Response;
use HTTP::Headers;
use HTML::TreeBuilder;
use HTML::TreeBuilder::XPath;
use Spreadsheet::ParseExcel;



#读取tool目录下的所有工具函数
my @toolname = <tool//*.pl>;
for(@toolname){
	require "$_";
}
#读取package下的所有测试函数
my @filename = <testcase//*.pl>;
for(@filename){
	require "$_";
}
#读取businessfunc下的所有测试函数
my @filename = <businessfunc//*.pl>;
for(@filename){
	require "$_";
}
#读取config下的所有测试函数
my @filename = <config//*.pl>;
for(@filename){
	require "$_";
}


#读取对应的功能脚本传入&myprint函数的控制命令和文件名&myprint("readFuncList","文件名")
my @funlist = &myprint("readFuncList","ReturnStartlist.txt");

#打开日志文件以便写入需要的信息
open LOG,">log.txt";
open RES,">>result.txt";

#展示方法名字并需要用户选取需要执行的函数
my %dohash;
for(@funlist){
	#截取字符串中的函数名
	my @ele = split;
	print "$ele[0]. $ele[1]. $ele[2]\n";
	$dohash{$ele[0].".".$ele[1].".".$ele[2]} = 0;
}

#读取用户输入的方法编号,并在hash的value中编辑需要执行标签 0不执行 1执行
chomp(my $enter = <STDIN>);
exit unless($enter =~ /\d/);	#输入不符合数字要求的都退出
my @spinter = split (/\s+/,$enter);

for my $mykey ( keys %dohash){
	my ($number,$fun) = split (/\./,$mykey);
	for my $userneed (@spinter){
		if($userneed == $number){
			$dohash{$mykey} = 1;
		}
	}
}

if($enter eq "all"){
	for ( keys %dohash){
		$dohash{$_}=1;
	}
}

#读取功能脚本中的同名函数，如果函数与功能函数不同名将无法运行
print RES localtime() . "($enter)" . " -- ";
for my $mykey1 (sort {$a <=> $b} keys %dohash){
	next unless($dohash{$mykey1});
	my ($number1,$fun1,$description) = split (/\./,$mykey1);
	print "TestCase >>$fun1 $description<< start\n";
	my $a1 = \&$fun1;	#通过引用来获取需要执行的函数名
	my $result = &$a1($number1);
	if($result eq 1){	#返回1 测试通过
		print "Test    VV PASS VV !!\n";
		print RES "PASS " ;
	}
	if($result eq 0){	#返回0 测试失败
		print "Test   XX FAILURE XX !!\n";
		print RES "FAILURE " ;
	}
	if($result eq -1){	#返回-1 测试结束不代表通过或失败
		print "Test   XX END XX !!\n";
		print RES "END " ;
	}
	print "TestCase >>$fun1<< end\n\n";
	sleep 1 ;
}
print RES "\n";

