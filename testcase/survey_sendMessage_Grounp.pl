use strict;

#加载businessfunc/survey_sendMessage下的所有函数
my @filename = <businessfunc//survey_sendMessage//*.pl>;
require "$_" for(@filename);


sub survey_sendMessage_Grounp
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#被测用户账号
	my @username = &readLine('1407996376_5_survey_0305.txt');
	#开始获取被测用户id
	my @user_id;
	for(@username){
		my $sql = "SELECT USER_ID FROM `USER_ACCOUNT` WHERE LOGIN_NAME='$_';";
		push @user_id, &operate_mysql(@survey,"$sql","select");
	}

	#创建用户组文件
	open WCSV,">data//1.csv";
	my @useridIndex = (0,1);
	for(@useridIndex){
		print WCSV "$user_id[$_]\n";
	}
	close WCSV;
	
	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"admin");
	
	#用户组id
	my $recipientid = &saveGroup($mgtbrowser,$mgt_addr,'data//1.csv');	
	print "recipientid -- $recipientid\n";
	

	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	########
	#给用户组发送消息
	########
	#testcase1 给用户组发送消息;
	my $testcase = "sendMessageGroup";
	#发送消息
	my $ms_tital = "yxmtital".time();	#消息标题,用时间戳来标记唯一
	my $ms_info = "haha http://www.baidu.com haha";
	#my $ms_info = "yxminfo".time();	#消息内容
	print "ms_tital:$ms_tital\n";
	&sendmessage($mgtbrowser,$mgt_addr,$ms_info,5,$recipientid,$ms_tital);
	#点击发送
	&clicksend($mgtbrowser,$mgt_addr,5,$ms_tital);
	
	#################
	#收消息并验证消息
	#################
	my @verif;	#验证数组,全1才算通过
	#手机端的登录请求
	my ($mobrowser,$accessToken,$userIndex);
	$userIndex = 0;
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[$userIndex],'MTExMTExMTE=');
	#收消息并验证消息
	print "\nUserId = $user_id[$userIndex]\n";
	push @verif,&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase);
	
	#手机登录请求
	$userIndex = 1;
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[$userIndex],'MTExMTExMTE=');
	#收消息并验证消息
	print "\nUserId = $user_id[$userIndex]\n";
	push @verif,&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase);
	
	#手机登录请求
	$userIndex = 2;
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[$userIndex],'MTExMTExMTE=');
	#收消息并验证消息
	print "\nUserId = $user_id[$userIndex]\n";
	if(&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase)){
		print "该用户不属于perltest用户组也收到了消息,测试失败\n";
		push @verif,0;
	}else{
		print "该用户不属于perltest用户组没有收到了消息,测试通过\n";
		push @verif,1;
	}
	
	#手机登录请求
	$userIndex = 3;
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[$userIndex],'MTExMTExMTE=');
	#收消息并验证消息
	print "\nUserId = $user_id[$userIndex]\n";
	if(&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase)){
		print "该用户不属于perltest用户组也收到了消息,测试失败\n";
		push @verif,0;
	}else{
		print "该用户不属于perltest用户组没有收到了消息,测试通过\n";
		push @verif,1;
	}
	
	#手机登录请求
	$userIndex = 4;
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[$userIndex],'MTExMTExMTE=');
	#收消息并验证消息
	print "\nUserId = $user_id[$userIndex]\n";
	if(&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase)){
		print "该用户不属于perltest用户组也收到了消息,测试失败\n";
		push @verif,0;
	}else{
		print "该用户不属于perltest用户组没有收到了消息,测试通过\n";
		push @verif,1;
	}
	
	
	
	#循环检查结果列表,并返回测试结果
	for(@verif){
		if($_ == 0){
			print "给用户组发送消息,部分验证错误\n";
			return 0 ;
		}
	}
	print "给用户组发送消息,测试全部验证正确\n";
	return 1;

}
1;