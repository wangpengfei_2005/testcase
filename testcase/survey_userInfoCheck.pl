#输出具体用户信息到文件

use strict;

sub survey_userInfoCheck
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件(mob_reg这个测试用例可以创建用户群)
	my @acc_file = ("1406527741_100_survey_0305.txt");
	#被测试的问卷id
	my $surveyid = "12597";
	
	#加载已经创建的用户列表
	for my $accfile (@acc_file){
		if(open READ,"account//$accfile"){
			print "account目录下 $accfile 文件成功打开\n开始读取\n";
		}else{
			die "account目录下无法打开 $accfile 文件\n";
		}
		my @userlist = <READ>;
		
		#######
		#手机端模拟用户答题的请求
		#######
		
		#开始循环单个文件中的用户列表
		for my $useraccount (@userlist){
			next if($useraccount =~ /^#/);	#
			&checkUserInfo($mob_addr,\@survey,$useraccount,$surveyid);
		}
		
	}
	
	return -1;
}
1;