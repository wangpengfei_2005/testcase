﻿use strict;


sub open_question
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	#开放平台地址
	my $open_addr = &config("","open_addr");
	
	#初始化一些信息

	
	#配额
	my $surveyCount_1 = "20";
	#myprint("encode", "问卷的配额总量? ");
	#chomp(my $surveyCount_1 = <STDIN>);
	myprint("encode", "开始创建问卷,配额:$surveyCount_1 \n");

	#开放平台登录请求
	my ($openbrowser);
	($openbrowser) = &openlogin($open_addr,'grshrd49@qq.com');

	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	#for(my $i=0; $i<100; $i++){
	########
	#创建问卷
	########
	#初始化数据
	my %parameter_group = (
		paperImage => '',
		surveyTitle => time()."_open",
		myFile => [''],
		surveyDesc => 'open_desc',
		flagIdentityAuth => '',
		flagPhoneAuth => '',
		
	);
	#执行创建问卷函数,并返回surveyid
	my $surveyid = &editSurvey($openbrowser,$open_addr,\%parameter_group);
	
	
	
	##########
	#editQuest
	##########
	#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频)
	#@Options 双数下标的元素可以键入图片路径用于上传图片
	#初始化创建题目的基本数据
	my (%questInfo,@Options,$type,$sort,$isRefQution,@questid,$qid);
	$sort = 1;				#排序从1开始
	$isRefQution = 0;		#默认都不是引用题
	@questid;				#存放qid
	#题目类型字典
	my %questType = (
		"1" => "单选题",
		"2" => "多选题",
		"3" => "尺度题",
		"4" => "文本题-数字",
		"5" => "文本题-文字",
		"6" => "图片题",
		"7" => "视频题",
		"8" => "语音题",
		"9" => "定位题",
		"10" => "web页面题",
	);
	
	
	#单多选
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '1',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
		'allowWord' => "0",	#单多选题的其它
	);
	#选项数据 [0][1]是题目标题 后面以此是问题标题,图片路径
	@Options = (	
		"标题",'',
		"选项1",'',
		"选项2","",
		"选项3",''
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;
	
	#单多选
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '2',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
		'allowWord' => "1",	#单多选题的其它
	);
	#选项数据 [0][1]是题目标题 后面以此是问题标题,图片路径
	@Options = (	
		"标题",'',
		"选项1",'',
		"选项2","",
		"选项3","",
		"选项4","",
		"选项5",''
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;
	
	
	#单多选
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '2',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
		'allowWord' => "1",	#单多选题的其它
	);
	#选项数据 [0][1]是题目标题 后面以此是问题标题,图片路径
	@Options = (	
		"标题",'',
		"选项1",'',
		"选项2","",
		"选项3","",
		"选项4","",
		"选项5",''
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;
	
	#单多选
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '2',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
		'allowWord' => "1",	#单多选题的其它
	);
	#选项数据 [0][1]是题目标题 后面以此是问题标题,图片路径
	@Options = (	
		"标题",'',
		"选项1",'',
		"选项2","",
		"选项3","",
		"选项4","",
		"选项5",''
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;
	
	#单多选
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '2',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
		'allowWord' => "1",	#单多选题的其它
	);
	#选项数据 [0][1]是题目标题 后面以此是问题标题,图片路径
	@Options = (	
		"标题",'',
		"选项1",'',
		"选项2","",
		"选项3","",
		"选项4","",
		"选项5",''
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;
	
	#尺度
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '3',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
	);
	#选项数据 [0][1]是题目标题 后面以此是问题标题,图片路径
	@Options = (
		"标题",'',
		"scaleNum",'9',	#3 5 7 9
		"leftDesc","左边描述",
		"rightDesc","右边描述"
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;

	#数字
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '4',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
	);
	#数字文字只有标题
	@Options = (
		"标题",'',
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;
	
	#文字
	#问题的基本信息 包括,类型$type,$surveyid,序号$sort 是否是引用题$isRefQution
	%questInfo = (
		'type' => '5',
		'surveyid' => $surveyid,
		'sort' => $sort++,
		'isRefQution' => $isRefQution,
	);
	#数字文字只有标题
	@Options = (
		"标题",'',
	);
	$qid = &editQuest($openbrowser,$open_addr,\%questType,\%questInfo,\@Options);
	push @questid,$qid;

	
	
	
	#########
	#逻辑设置 or and to end  select noselect
	#########
	my @logicList = (
		#"q2-select-2 end",
		#"q1-select-2 and q2-select-3 and q2-select-4 to q4",
	);
	&editLogical($openbrowser,$open_addr,$surveyid,\@logicList,\@questid);
	
	

	########
	#创建配额
	########
	#1女  0男
	my (@quota_1, @quota_2);
	#提交一个
	@quota_1 = (
		'age',"",,
		'area',"",
		'sex','',
		'surveyCount',$surveyCount_1,
		'surveyId',$surveyid,
	);
	@quota_2 = ();	#或许以后会在配额这里做2次请求
	&editQuota($openbrowser,$open_addr,$surveyid,\@quota_1,\@quota_2);

	
	#####
	#设置奖励
	#####
	my @bonus_post = (
		'surveyId' , $surveyid,
		'awardAmount' , "10",
		'awardTypeStr' , "1",	#1.现金奖励  2.实物奖励
		'awardItemName' , "fffff",
		'awardItemPrice' , "2",
	);
	&editBonus($openbrowser,$open_addr,$surveyid,\@bonus_post);

	####
	#提交问卷
	&Opensubmit($openbrowser,$open_addr,$surveyid);
	
	
	
	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,'');
	
	
	#确认受理http://10.201.2.12/survey/a/acceptSurvey?surveyId=2824
	&acceptSurvey($mgtbrowser,$mgt_addr,$surveyid);
	
	
	####
	#审核上家
	####
	my %auditconfig = ();
	&audit($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);

	####
	#立刻上架操作
	####
	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","10");
	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
	&operate_mysql(@survey,"$sql","update");

	#}
	#######
	#手机端验证是否存在该问卷
	#######
	return &checkMySurvey($mob_addr,$surveyid,'15825670001');

}
1;