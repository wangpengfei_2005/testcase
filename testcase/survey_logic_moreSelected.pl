use strict;

sub survey_logic_moreSelected
{
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#初始化一些信息和资源
	#数据库链接信息
	#my @survey = ("survey_1205","10.201.2.13","3306","mq-user","redhat");
	my @survey = ("survey_0305","10.201.2.13","3306","mq-user","redhat");

	#管理后台地址
	#my $mgt_addr = "http://10.201.2.12";
	my $mgt_addr = "http://10.201.70.81";
	my @user_id = ("10301","10101","10293");
	#手机服务地址
	#my $mob_addr = "http://10.201.2.12:8080";
	my $mob_addr = "http://10.201.70.82:8080";
	my @username = ("15021809097","15900826057","15715559622");
	
	#全局读取的文件名
	my $acc_file = "1355694_50_survey_0305.txt";
	
	#三端代码负责这条测试用例的不同功能，用一个数组来控制不同的模块是否运行
	#创建问卷、用户作答、验证答卷
	my @case = (1,1,1);
	
	#################
	#开始创建测试问卷
	#################
	my ($surveyid,$questidRef,@questid,$mgtbrowser) ;
	if($case[0]){
		#设置逻辑之前的一些操作返回sid 还有题目编号
		#测试数据:A1，A1A2 ，A1A3，A1A4，A1A2A3，A1A2A4，A1A3A4，A1A2A3A4
		my $description = '描述:A1跳转到Q3';
		#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频) 这里只能统一所有题目类型
		my $questionType = "2";
		($surveyid,$questidRef,$mgtbrowser) = &createSurvey_1($mgt_addr,$description,$questionType);
		@questid = @$questidRef;
	
		#relationCode 并且2  或者1   selected 选中1  不选中0
		my @logichash = (
			'surveyId',"$surveyid",
			'logicalUnits[0].conditions[0].qid',"$questid[0]",
			'logicalUnits[0].conditions[0].optIds',"1",
			'logicalUnits[0].conditions[0].selected',"1",

			'logicalUnits[0].result.qid',$questid[2],
			'logicalUnits[0].actionName','JUMP',
		);
		&createSurvey_2($mgt_addr,$mob_addr,$surveyid,\@logichash,$mgtbrowser,\@questid,\@survey,\@username);
		
		
		#################
		#开始虚拟用户答题
		#根据用户来循环，遇到最后一个答案就跳出答题循环
		#################

		my @returnUser;#接收用户答题信息
		&myprint("encode", "开始虚拟用户答题\n");
		
		#需要读取的账号文件(mob_reg这个测试用例可以创建用户群)
		#my $acc_file = "1358086_10_survey_0305.txt";
		if($acc_file eq ""){
			&myprint("encode", "输入account目录下的一个用户列表文件名:");
			chomp(my $acc_file = <STDIN>);
		}

		#只有在单独执"虚拟用户答题"代码块时需要用户来输入
		if($surveyid eq ""){
			&myprint("encode", "输入用户需要作答的问卷id");
			chomp($surveyid = <STDIN>);
		}
		
		#加载已经创建的用户列表
		if(open READ,"account//$acc_file"){
			&myprint("encode", "account目录下 $acc_file 文件成功打开\n开始读取\n");
		}else{
			die "Can not open file -> $acc_file \n";
		}
		my @userlist = <READ>;
		&myprint("encode", "被测问卷id:$surveyid\n");
		
		#手机端模拟用户答题的请求 
		#mode1普通随机答题
		#mode2(设置有超时用户和被终结用户)
		#mode3指定用户的答题列表
		my $mode = 3;
		my @answerList = (
			'1;2;3;4;5',
			'1,2;2;3;4;5',
			'1,3;2;3;4;5',
			'1,4;2;3;4;5',
			'1,2,3;2;3;4;5',
			'1,3,4;2;3;4;5',
			'1,2,3,4;2;3;4;5',
		);	#每一个元素代表一个用户的答题列表
		@returnUser = &userAnswer($mob_addr,\@survey,\@userlist,$surveyid,$mode,\@answerList);

		

	}
	
	
	
	
	if($case[1]){
		#设置逻辑之前的一些操作返回sid 还有题目编号
		#测试数据:A1,A1A2,A1A3,A1A4,A1A2A3,A1A2A4,A1A3A4,A1A2A3A4,A2,A2A3,A2A4,A2A3A4
		my $description = '描述:2.A1或者A2跳转到Q3';
		#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频) 这里只能统一所有题目类型
		my $questionType = "2";
		($surveyid,$questidRef,$mgtbrowser) = &createSurvey_1($mgt_addr,$description,$questionType);
		@questid = @$questidRef;
	
		#relationCode 并且2  或者1   selected 选中1  不选中0
		my @logichash = (
			'surveyId',"$surveyid",
			'logicalUnits[0].conditions[0].qid',"$questid[0]",
			'logicalUnits[0].conditions[0].optIds',"1",
			'logicalUnits[0].conditions[0].selected',"1",
			
			'logicalUnits[0].conditions[1].qid',"$questid[0]",
			'logicalUnits[0].conditions[1].optIds',"2",
			'logicalUnits[0].conditions[1].selected',"1",
			'logicalUnits[0].relationCode',"1",
			
			'logicalUnits[0].result.qip',$questid[2],
			'logicalUnits[0].actionName','JUMP',
		);

		&createSurvey_2($mgt_addr,$mob_addr,$surveyid,\@logichash,$mgtbrowser,\@questid,\@survey,\@username);
		
		
		
		#################
		#开始虚拟用户答题
		#根据用户来循环，遇到最后一个答案就跳出答题循环
		#################

		my @returnUser;#接收用户答题信息
		&myprint("encode", "开始虚拟用户答题\n");
		
		#需要读取的账号文件(mob_reg这个测试用例可以创建用户群)
		#my $acc_file = "1358086_10_survey_0305.txt";
		if($acc_file eq ""){
			&myprint("encode", "输入account目录下的一个用户列表文件名:");
			chomp(my $acc_file = <STDIN>);
		}

		#只有在单独执"虚拟用户答题"代码块时需要用户来输入
		if($surveyid eq ""){
			&myprint("encode", "输入用户需要作答的问卷id");
			chomp($surveyid = <STDIN>);
		}
		
		#加载已经创建的用户列表
		if(open READ,"account//$acc_file"){
			&myprint("encode", "account目录下 $acc_file 文件成功打开\n开始读取\n");
		}else{
			die "Can not open file -> $acc_file \n";
		}
		my @userlist = <READ>;
		&myprint("encode", "被测问卷id:$surveyid\n");
		
		#手机端模拟用户答题的请求 
		#mode1普通随机答题
		#mode2(设置有超时用户和被终结用户)
		#mode3指定用户的答题列表
		my $mode = 3;
		my @answerList = (
			'1;2;3;4;5',
			'1,2;2;3;4;5',
			'1,3;2;3;4;5',
			'1,4;2;3;4;5',
			'1,2,3;2;3;4;5',
			'1,2,4;2;3;4;5',
			'1,3,4;2;3;4;5',
			'1,2,3,4;2;3;4;5',
			'2;2;3;4;5',
			'2,3;2;3;4;5',
			'2,4;2;3;4;5',
			'2,3,4;2;3;4;5'
		);	#每一个元素代表一个用户的答题列表
		@returnUser = &userAnswer($mob_addr,\@survey,\@userlist,$surveyid,$mode,\@answerList);

	
	}
	
	
	
	
	if($case[2]){
		#设置逻辑之前的一些操作返回sid 还有题目编号
		#测试数据:A1A2,A1A2A3,A1A2A4,A1A2A3A4
		my $description = '描述:A1并且A2跳转到Q3';
		#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频) 这里只能统一所有题目类型
		my $questionType = "2";
		($surveyid,$questidRef,$mgtbrowser) = &createSurvey_1($mgt_addr,$description,$questionType);
		@questid = @$questidRef;
	
		#relationCode 并且2  或者1   selected 选中1  不选中0
		my @logichash = (
			'surveyId',"$surveyid",
			'logicalUnits[0].conditions[0].qid',"$questid[0]",
			'logicalUnits[0].conditions[0].optIds',"1",
			'logicalUnits[0].conditions[0].selected',"1",
			
			'logicalUnits[0].conditions[1].qid',"$questid[0]",
			'logicalUnits[0].conditions[1].optIds',"2",
			'logicalUnits[0].conditions[1].selected',"1",
			'logicalUnits[0].relationCode',"2",
			
			
			'logicalUnits[0].result.qid',$questid[2],
			'logicalUnits[0].actionName','JUMP',
		);

		&createSurvey_2($mgt_addr,$mob_addr,$surveyid,\@logichash,$mgtbrowser,\@questid,\@survey,\@username);
		
		
		#################
		#开始虚拟用户答题
		#根据用户来循环，遇到最后一个答案就跳出答题循环
		#################

		my @returnUser;#接收用户答题信息
		&myprint("encode", "开始虚拟用户答题\n");
		
		#需要读取的账号文件(mob_reg这个测试用例可以创建用户群)
		#my $acc_file = "1358086_10_survey_0305.txt";
		if($acc_file eq ""){
			&myprint("encode", "输入account目录下的一个用户列表文件名:");
			chomp(my $acc_file = <STDIN>);
		}

		#只有在单独执"虚拟用户答题"代码块时需要用户来输入
		if($surveyid eq ""){
			&myprint("encode", "输入用户需要作答的问卷id");
			chomp($surveyid = <STDIN>);
		}
		
		#加载已经创建的用户列表
		if(open READ,"account//$acc_file"){
			&myprint("encode", "account目录下 $acc_file 文件成功打开\n开始读取\n");
		}else{
			die "Can not open file -> $acc_file \n";
		}
		my @userlist = <READ>;
		&myprint("encode", "被测问卷id:$surveyid\n");
		
		#手机端模拟用户答题的请求 
		#mode1普通随机答题
		#mode2(设置有超时用户和被终结用户)
		#mode3指定用户的答题列表
		my $mode = 3;
		my @answerList = (
			'1,2;2;3;4;5',
			'1,2,3;2;3;4;5',
			'1,2,4;2;3;4;5',
			'1,2,3,4;2;3;4;5'
		);	#每一个元素代表一个用户的答题列表
		@returnUser = &userAnswer($mob_addr,\@survey,\@userlist,$surveyid,$mode,\@answerList);
		

	}

}

1;