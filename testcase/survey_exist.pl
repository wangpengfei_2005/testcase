﻿use strict;


sub survey_exist
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#初始化一些信息
	my @user_id = ("10301","10101","10293");
	my @username = ("15021809097","15900826057","15715559622");


	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = mgtlogin($mgt_addr,"yangxianming1");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#创建问卷
	########
	#初始化数据
	my %parameter_group = (
		paperImage=>"",
		awardItemImage=>"",
		myfile=>"",
		projectCode=>"perltest",
		surveyTitle=>"验证普通问卷是否上架_".time()."_perlscript",
		#surveyTitle=>"",
		surveyDesc=>"perlscript create survey_描述:上架成功后。登陆手机客户端查看此问卷是否存在",
		flagVideo=>"1",				#带视频(1.带视频  空:不带视频)
		hasCargo=>"0",				#物流(1.需要  0.不需要)
		lotteryTypeStr=>'0',
		quotaProportion=>'',
		participationProportion=>'',
		presentName=>"",
		presentAmount=>"",
		presentCount=>"",
		awardTypeStr=>"1",			#1.现金 2.非现金 3.红包
		awardAmount=>"1",
		averageAward=>"0",
		sigelHighAward=>"0",
		sigelLowAward=>"0",
		awardItemName=>"",
		awardItemPrice=>"",
		awardItemNum=>"",
		awardItemDesc=>"",
		awardImagefile=>"",
		surveyHelp=>"",
		flagBooking=>"0",			#是否是未来任务(1.是  0.否)
		flagIdentityAuth=>"",		#需要身份验证(1.需要  空:不需要)
		flagPhoneAuth=>"",			#需要手机验证(1.需要  空:不需要)
	);
	#执行创建问卷函数,并返回surveyid
	my $surveyid = &addsurvey($mgtbrowser,$mgt_addr,\%parameter_group);
	
	
	
	##############################
	#创建edit_question
	#尺度题数组@Options格式：("尺度大小3|5|7|9","左描述","右描述")
	#数字填空，文字填空，需要清空数组@Options
	#视频题@Options格式：("视频文件的本地地址")
	#将数组传入createquest函数
	#creatquest函数格式&createquest(http对象,管理后台地址,题目类型,$surveyid,序号,题目名,题目图片文件名,数组引用);
	#############################
	
	#申明并初始化部分数据
	#@options (选项名字,选项图片,..) 的规则排列
	#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频)
	my (@Options,$type,$sort,$flagReference,@questid,$temp);
	$sort = 1;				#排序从1开始
	$flagReference = 0;		#默认都不是引用题
	@questid;		#引用题的引用源
	
	
	
	#初始化数据(单选)
	
	for(my $i=0; $i<10; $i++){
		@Options = ("danxuanxuanxiang11","",
						"danxuanxuanxiang12","",
						"danxuanxuanxiang13","",
						"danxuanxuanxiang14","",
						"danxuanxuanxiang15","");
		$type = 1;	
		#执行创建题目函数(一次创建一个题目)
		$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
		push @questid,$temp;
	}

	@Options = ("danxuanxuanxiang11","",
						"danxuanxuanxiang12","",
						"danxuanxuanxiang13","",
						"danxuanxuanxiang14","",
						"danxuanxuanxiang15","");
	$type = 1;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	

	
#	#初始化数据(多选)
#	@Options = ("xuanxiang11","resources//Multiple_a1.jpg",
#					"xuanxiang12","resources//Multiple_a1.jpg",
#					"xuanxiang13","",
#					"xuanxiang14","",
#					"xuanxiang15","",
#					"xuanxiang16","resources//Multiple_a1.jpg");
#	$type = 2;	
#	#执行创建题目函数(一次创建一个题目)
#	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
#	push @questid,$temp;
#
#	
#	#初始化数据(尺度)
#	@Options = ("5","left","right");
#	$type = 3;	
#	#执行创建题目函数(一次创建一个题目)
#	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
#	push @questid,$temp;
#	
#	#初始化数据(数字题)
#	@Options = ();
#	$type = 4;	
#	#执行创建题目函数(一次创建一个题目)
#	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
#	push @questid,$temp;
#	
#	#初始化数据(文字题)
#	@Options = ();
#	$type = 5;	
#	#执行创建题目函数(一次创建一个题目)
#	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
#	push @questid,$temp;
#
#	
#	#初始化数据(引用题,单选题)
#	@Options = ();
#	$type = 1;	
#	$flagReference = 1;	#引用题
#	my @refquest = (1,2);	#引用题引用的题号
#	#执行创建题目函数(一次创建一个题目,目前只支持引用一题)
#	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"","",\@Options,$flagReference,\@questid,\@refquest);
#	$flagReference = 0;	#清空引用题标识
#	push @questid,$temp;
	

	
	
	##########
	#创建逻辑
	#########
	#typeName = TERMINATION,JUMP
	my %logichash;
	%logichash = (
		'surveyId'=>$surveyid,
	);
	#一个逻辑
	$logichash{'logicalUnits[0].conditions[0].optIds'}=1;
	$logichash{'logicalUnits[0].conditions[0].qid'}=$questid[1];
	$logichash{'logicalUnits[0].conditions[0].selected'}=1;
	$logichash{'logicalUnits[0].result.qid'}=$questid[$#questid];
	$logichash{'logicalUnits[0].actionName'}='JUMP';
	#一个逻辑
	$logichash{'logicalUnits[1].conditions[0].optIds'}=1;
	$logichash{'logicalUnits[1].conditions[0].qid'}=$questid[0];
	$logichash{'logicalUnits[1].conditions[0].selected'}=1;
	$logichash{'logicalUnits[1].actionName'}='TERMINATION';

	#调试
	#while(my($k,$v) = each %logichash){
	#	print "debug---$k,$v\n";
	#}
#	&editlogic($mgtbrowser,$mgt_addr,\%logichash);
	
	
	
	########
	#创建配额
	########
	#1女  0男
	my (@quota_1, @quota_2);
	#提交一个
	@quota_1 = (
		'age',"",
		'amount',"",
		'area',"",
		'reviewer','213',
		'reviewer','213',
		'sex','',
		'surveyCount',11,
		'surveyId',$surveyid,
		);
	@quota_2 = (
		'quotaType',"1",
		'totalSamples',"",
		'surveyId',$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,$surveyid,\@quota_1,\@quota_2);
	

	####
	#提交问卷
	#survey/a/submitSurvey?surveyId=3150
	&submit($mgtbrowser,$mgt_addr,$surveyid);
	
	####
	#审核上家
	####
	my %auditconfig = ();
	&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
	

	
	####
	#立刻上架操作
	####
	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","10");
	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
	&operate_mysql(@survey,"$sql","update");
	sleep 1;



	#######
	#手机端验证是否存在该问卷
	#######
	#return &checkMySurvey($mob_addr,$surveyid,$username[0]);
	
	
	
}
1;