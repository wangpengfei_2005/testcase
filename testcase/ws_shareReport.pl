#点击抽奖

use strict;

sub ws_shareReport
{
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	#开放平台地址
	my $open_addr = &config("","open_addr");
	#wap地址
	my $wap_addr = &config("","wap_addr");
	#广告系统地址
	my $ad_addr = &config("","ad_addr");
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	#初始化被分享用户的browser
	my $ck=HTTP::Cookies->new();
	my $shareBrowser=LWP::UserAgent->new(agent=>'Mozilla/5.0',cookie_jar=>$ck);
	
	#获取测试数据	$data{'字段名字'}->[第几行数据的下标]  $data{'userAccount'}->[n] 
	my %data = &readExcel('1413188043_1000_survey_0305.xls','Sheet1');
	my $maxLine = $data{'maxDataLine'};
	
	my @shareUrlArr;
	for(0..$maxLine){
		next unless($data{'doit'}->[$_]);
		my $userAccount = $data{'account'}->[$_];
		my $reportId = $data{'reportId'}->[$_];
		my $shareCount = $data{'shareCount'}->[$_];
		
		$shareCount = &random(1,100) if($shareCount eq 'random');
		
		my ($mobrowser,$accessToken);
		#手机登录
		($mobrowser,$accessToken) = &moblogin($mob_addr,$userAccount,'MTExMTExMTE=');
		next unless($mobrowser);
		print "userAccount:$userAccount , Token:($accessToken) , ";
		
		#执行分享微报告接口
		#my $reportId = "237";
		print "reportId:$reportId , ";
		my $Return = &shareReport($mob_addr, \@survey, $mobrowser, $accessToken, $reportId);
		myprint("encode", "LotteryReturn:($Return) \n");
		
		#从分享报告的返回的json中提取分享链接
		$json_obj = $json->decode("$Return");
		my $shareUrl = $json_obj->{'data'}->{'shareUrl'};
		$shareUrl = $1 if($shareUrl =~ /(http:.*)/);
		print "shareUrl:$shareUrl \n";
		push @shareUrlArr,$shareUrl;
		
		#被分享用户提交shareUrl
		#my $shareBrowserRetrun = $shareBrowser->get("$shareUrl",)->content;
		#print RES "shareBrowserRetrun : $shareBrowserRetrun\n";
		
		#组装浏览页面地址,模拟js组装的统计浏览量的url
		my ($zoneid, $lgpage);
		if($shareUrl =~ /\?zoneid=(.*)/){
			$zoneid = $1;
		}
		$lgpage = $ad_addr . '/www/delivery/lg.php?';
		$lgpage = $lgpage . 'bannerid=1&campaignid=1&';
		$lgpage = $lgpage . 'zoneid='.$zoneid.'&';
		$lgpage = $lgpage . 'cb=acc941af58&';
		$lgpage = $lgpage . 'loc=http%3A%2F%2F10.201.70.82%3A8080%2FvReport%2Fv_report%2F'.$reportId.'$reportId'.$zoneid;
		#请求浏览量统计地址
		print "Start to review .. ";
		for my $c (1..$shareCount){
			my $shareBrowserRetrun = $shareBrowser->get("$lgpage",)->content;
			print " $c";
		}
		print "\n";
		#print  "shareBrowserRetrun : $shareBrowserRetrun\n";
		#myprint("encode", "shareBrowserRetrun : $shareBrowserRetrun\n");
	}
	
#	#撤销微报告发布
#	#管理后台登录请求
#	my ($mgtbrowser);
#	($mgtbrowser) = mgtlogin($mgt_addr,"yangxianming1");
#	
#	
#	#循环执行请求统计地址
#	for(0..$maxLine){
#		my $shareCount = $data{'shareCount'}->[$_];
#		my $reportId = $data{'reportId'}->[$_];
#		
#		#执行微报告下架操作
#		my $returnPut = &put_off_shelf($mgtbrowser,$mgt_addr,$reportId);
#		if($returnPut){
#			print "reportId:$reportId put_off success\n";
#		}else{
#			print "reportId:$reportId put_off failed \n";
#		}
#		sleep 1;
#		
#		#组装浏览页面地址,模拟js组装的统计浏览量的url
#		my ($zoneid, $lgpage);
#		if($shareUrlArr[$_] =~ /\?zoneid=(.*)/){
#			$zoneid = $1;
#		}
#		$lgpage = $ad_addr . '/www/delivery/lg.php?';
#		$lgpage = $lgpage . 'bannerid=1&campaignid=1&';
#		$lgpage = $lgpage . 'zoneid='.$zoneid.'&';
#		$lgpage = $lgpage . 'cb=acc941af58&';
#		$lgpage = $lgpage . 'loc=http%3A%2F%2F10.201.70.82%3A8080%2FvReport%2Fv_report%2F'.$reportId.'$reportId'.$zoneid;
#		#请求浏览量统计地址
#		print "Start to review .. ";
#		for my $c (1..$shareCount){
#			my $shareBrowserRetrun = $shareBrowser->get("$lgpage",)->content;
#			print " $c";
#		}
#		print "\n";
#		#print  "shareBrowserRetrun : $shareBrowserRetrun\n";
#		#myprint("encode", "shareBrowserRetrun : $shareBrowserRetrun\n");		
#	
#	}
#	

	return -1;

}
1;