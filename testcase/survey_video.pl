﻿use strict;



sub survey_video
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	
	#初始化一些信息
	my @user_id = ("10301","10101","10293");
	my @username = ("15021809097","15900826057","15715559622");


	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"jiangw");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#创建问卷
	########
	#初始化数据
	my %parameter_group = (
		paperImage=>"",
		awardItemImage=>"",
		myfile=>"",
		projectCode=>"perltest",
		surveyTitle=>"视频题_".time()."_perlscript",
		surveyDesc=>'perlscript create survey_描述:勾选含有视频.问卷包含视频题1，"可跳过""可暂停,快进,快退"|问卷包含视频题2，"不可跳过""不可暂停,快进,快退"。调查简介页显示含有视频(主要验证手机端能否快进快退)',
		flagVideo=>"1",				#带视频(1.带视频  空:不带视频)
		hasCargo=>"0",				#物流(1.需要  0.不需要)
		lotteryTypeStr=>'0',
		quotaProportion=>'',
		participationProportion=>'',
		presentName=>"",
		presentAmount=>"",
		presentCount=>"",
		awardTypeStr=>"1",			#1.现金 2.非现金 3.红包
		awardAmount=>"1",
		averageAward=>"0",
		sigelHighAward=>"0",
		sigelLowAward=>"0",
		awardItemName=>"",
		awardItemPrice=>"",
		awardItemNum=>"",
		awardItemDesc=>"",
		awardImagefile=>"",
		surveyHelp=>"",
		flagBooking=>"0",			#是否是未来任务(1.是  0.否)
		flagIdentityAuth=>"",		#需要身份验证(1.需要  空:不需要)
		flagPhoneAuth=>"",			#需要手机验证(1.需要  空:不需要)
	);
	#执行创建问卷函数,并返回surveyid
	my $surveyid = &addsurvey($mgtbrowser,$mgt_addr,\%parameter_group);
	
	
	
	##############################
	#创建edit_question
	#尺度题数组@Options格式：("尺度大小3|5|7|9","左描述","右描述")
	#数字填空，文字填空，需要清空数组@Options
	#视频题@Options格式：("视频文件的本地地址")
	#将数组传入createquest函数
	#creatquest函数格式&createquest(http对象,管理后台地址,题目类型,$surveyid,序号,题目名,题目图片文件名,数组引用);
	#############################
	
	#申明并初始化部分数据
	#@options (选项名字,选项图片,..) 的规则排列
	#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频)
	my (@Options,$type,$sort,$flagReference,@questid,$temp);
	$sort = 1;				#排序从1开始
	$flagReference = 0;		#默认都不是引用题
	@questid;		#引用题的引用源
	
	
	
	#初始化数据(视频1)
	@Options = ("resources//www.mp4","1","1");
	$type = 7;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(视频2)
	@Options = ("resources//www.mp4","0","0");
	$type = 7;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	

	
	
	##########
	#创建逻辑
	##########
	#($typename,$surveyid,$fromquest,$selected,$toquest)
	#目前只支持单个逻辑与条件,单题跳转单选项跳转,termination,jump

	########
	#创建配额
	########
	#1女  0男
	my (@quota_1, @quota_2);
	#提交一个
	@quota_1 = (
		'age',"",
		'amount',"",
		'area',"",
		'reviewer','213',
		'reviewer','213',
		'sex','',
		'surveyCount',11,
		'surveyId',$surveyid,
		);
	@quota_2 = (
		'quotaType',"1",
		'totalSamples',"",
		'surveyId',$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,$surveyid,\@quota_1,\@quota_2);
	
	
	
	####
	#提交问卷
	####survey/a/submitSurvey?surveyId=3150
	&submit($mgtbrowser,$mgt_addr,$surveyid);
	
	####
	#审核上家
	####
	my %auditconfig = ();
	&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
	
	####
	#立刻上架操作
	####
	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","10");
	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
	&operate_mysql(@survey,"$sql","update");
	sleep 1;

	
	
	#######
	#手机端验证是否存在该问卷
	#######

	#手机端的登录请求
	my ($mobrowser,$accessToken);
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[0],'MTExMTExMTE=');
	
	
	#开始答题,返回第一题的列表
	my $return = $mobrowser->post("$mob_addr/survey-ws/surveysvc/seckill",
		{
		surveyid => $surveyid,
		addressid => '6377',
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
		)->content;
	#&myprint("encode", "seckill - $return\n");
	&verification("mob","survey_pic.surveys",$mobrowser,$return);
	##开始验证
	my @verif;					#验证数组,全1才算通过
	if($return =~ /"videoURL":"(.*?)"/i){
		&myprint("encode", "video url - $1\n");
	}
	if($return =~ /"pauseFlag":(.*?),"skipFlag":(.*?),/i){
		&myprint("encode", "pauseFlag:$1  skipFlag:$2\n");
		if($1 eq "true" && $2 eq "true"){push @verif,1;}
		else{push @verif,0;}
	}

	#第一题的回答,返回第二题的列表
	my $return = $mobrowser->get("$mob_addr/survey-ws/surveysvc/answerStep?surveyid=${surveyid}&questionid=1&optionid=1&stepType=2",
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
		)->content;
	#&myprint("encode", "submit - $return\n");
	&verification("mob","survey_pic.surveys",$mobrowser,$return);
	
	####
	#开始验证
	####
	if($return =~ /"videoURL":"(.*?)"/i){
		&myprint("encode", "video url - $1\n");
	}
	if($return =~ /"pauseFlag":(.*?),"skipFlag":(.*?),/i){
		&myprint("encode", "pauseFlag:$1  skipFlag:$2\n");
		if($1 eq "false" && $2 eq "false"){push @verif,1;}
		else{push @verif,0;}
	}
	
	#循环检查结果列表,并返回测试结果
	for(@verif){
		if($_ == 0){
			&myprint("encode", "视频设置验证不正确\n");
			return 0 ;
		}
	}
	&myprint("encode", "视频设置验证正确\n");
	return 1;
	
	
}
1;