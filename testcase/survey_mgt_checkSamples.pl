#测试excel报表数据是否一致


use strict;

sub survey_mgt_checkSamples
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#初始化一些信息和资源
	#数据库链接信息
	my @survey = ("survey_0305","10.201.2.13","3306","mq-user","redhat");
	#my $sql = "SELECT USER_ID FROM `USER_ACCOUNT` WHERE LOGIN_NAME = \"13564324375\"";
	#my $a = &operate_mysql(@survey,"$sql","select");
	my $dbh = &connection(@survey);
	#管理后台地址
	#my $mgt_addr = "http://10.201.2.12";
	my $mgt_addr = "http://10.201.70.81";
	my @user_id = ("10301","10101","10293");
	#手机服务地址
	#my $mob_addr = "http://10.201.2.12:8080";
	my $mob_addr = "http://10.201.70.82:8080";
	my @username = ("15021809097","15900826057","15715559622");


	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"jiangw");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	

	####
	#下载问卷统计报告
	####survey/a/submitSurvey?surveyId=3150,3625
	my $surveyid = "3369"; #3369
	my $html;
	$html = &download_file($mgtbrowser,"$mgt_addr/check/downLoadSamples/$surveyid/samples");
	
	#####
	#分析报告
	####
	#获取下载文件中需要的数据
	my %get_htmlhash;	#存放文件数据，方便后数据库的数据做对比
	my @listtop = &xpath_find_value($html,'/html/body/table/tbody/tr[2]/td[position()>2]');
	#xpath中@符号可以获取属性值。listtop_length接收属性的列表
	my @listtop_length = &xpath_find_value($html,'/html/body/table/tbody/tr[2]/td[position()>2]/@colspan');	
	my @listname = &xpath_find_value($html,'/html/body/table/tbody/tr[3]/td');
	my @list1 = &xpath_find_value($html,'/html/body/table/tbody/tr[4]/td[position()>2]');
	my @list2 = &xpath_find_value($html,'/html/body/table/tbody/tr[5]/td[position()>2]');
	my @listtop_list;	#这个组装统计指标到数组，以便可以识别多个重复的"其他"统计指标
	for(my $i=0; $i<=$#listtop_length; $i++){
		for(my $from=1; $from<=$listtop_length[$i]; $from++){
			push @listtop_list,$listtop[$i];
		}
	}
	#print "$_ " for(@listtop_list);
	for(my $i=0; $i<=$#listname; $i++){
		$get_htmlhash{$listtop_list[$i]."_".$listname[$i]} = $list2[$i];
		#print "$listname[$i]  $list1[$i]  $list2[$i]\n";
	}
	
	
	###获取数据库中该报告的数据
	my %get_sqlhash;	#存放从数据库查出来的数据
	%get_sqlhash = &get_quota_data($surveyid,$dbh);
	
	#print "debug----\n";
	#foreach my $key (sort {$a<=>$b} keys %get_htmlhash){  
	#	print "html  --  key:$key  htmlvalue:$get_htmlhash{$key}--sqlvalue:$get_sqlhash{$key} \n";
	#}
	#print "debug----\n";


	#分析html和sql查出来的两个hash列表是否相同
	my @verif;	#验证数组,全1才算通过
	#首先验证两个hash列表的key个数是否相同
	if((keys %get_htmlhash) == (keys %get_sqlhash)){
		print "html和sql的字段数量一致\n";push @verif,1;
	}else{
		print "html和sql的字段数量不一致\n";push @verif,0;
		#比对是那个指标缺省
		my %max_hash = (keys %get_htmlhash)>(keys %get_sqlhash) ? %get_htmlhash : %get_sqlhash;
		for my $key (keys %max_hash){
			print "key:$key unexists in download file\n" unless(exists $get_htmlhash{$key});
			print "key:$key unexists in database\n" unless(exists $get_sqlhash{$key});
		}
		return 0 ;
	}
	
	#在key个数相同的前提下逐个验证value是否相同
	for my $key (keys %get_htmlhash){
		if($get_htmlhash{$key} == $get_sqlhash{$key}){
			push @verif,1;
		}else{
			print "hash的value出现异常\n";
			print "key:$key  htmlvalue=$get_htmlhash{$key} sqlvalue=$get_sqlhash{$key}\n";
			push @verif,0;
		}
	}
	
	#循环检查结果列表,并返回测试结果
	for(@verif){
		if($_ == 0){
			print "样本完成情况统计导出,部分验证错误\n";
			return 0 ;
		}
	}
	print "样本完成情况统计导出文件与数据库一致,测试全部验证正确\n";
	return 1;
	
	#调试代码
	#foreach my $key (sort {$a<=>$b} keys %h){  
	#	print "key:$key  value:$h{$key} \n";
	#}

}
1;