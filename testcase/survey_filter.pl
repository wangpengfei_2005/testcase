#该测试 前2男性账号，后1女性账号。问卷配额2份每份1个，男女各一份

sub survey_filter
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");

	my @username = ("13564324371","13564324372","13564324373");
	my @user_id = ("10301","10101","10293");

	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"jiangw");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#创建问卷
	########
	#初始化数据
	my %parameter_group = (
		paperImage=>"",
		awardItemImage=>"",
		myfile=>"",
		projectCode=>"perltest",
		surveyTitle=>time()."_perlscript",
		surveyDesc=>"perlscript create survey",
		flagVideo=>"1",				#带视频(1.带视频  空:不带视频)
		hasCargo=>"",				#物流(1.需要  0.不需要)
		presentName=>"",			#物流名字
		presentAmount=>"",			#物流的价值
		presentCount=>"",			#物流的数量
		awardTypeStr=>"1",
		awardItemName=>"",
		awardItemPrice=>"",
		awardItemNum=>"",
		awardItemDesc=>"",
		awardImagefile=>"",
		surveyHelp=>"",
		flagBooking=>"",			#是否是未来任务(1.是  0.否)
		flagIdentityAuth=>"",		#需要身份验证(1.需要  空:不需要)
		flagPhoneAuth=>"",			#需要手机验证(1.需要  空:不需要)
	);
	#执行创建问卷函数,并返回surveyid
	my $surveyid = &addsurvey($mgtbrowser,$mgt_addr,\%parameter_group);
	
	
	
	##############################
	#创建edit_question
	#尺度题数组@Options格式：("尺度大小3|5|7|9","左描述","右描述")
	#数字填空，文字填空，需要清空数组@Options
	#视频题@Options格式：("视频文件的本地地址")
	#将数组传入createquest函数
	#creatquest函数格式&createquest(http对象,管理后台地址,题目类型,$surveyid,序号,题目名,题目图片文件名,数组引用);
	#############################
	
	#申明并初始化部分数据
	#@options (选项名字,选项图片,..) 的规则排列
	#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频)
	my (@Options,$type,$sort,$flagReference,@questid,$temp);
	$sort = 1;				#排序从1开始
	$flagReference = 0;		#默认都不是引用题
	@questid;		#引用题的引用源
	
	#初始化数据(单选)
	@Options = ("danxuanxuanxiang11","",
					"danxuanxuanxiang12","",
					"danxuanxuanxiang13","",
					"danxuanxuanxiang14","",
					"danxuanxuanxiang15","");
	$type = 1;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;


	#初始化数据(多选)
	@Options = ("xuanxiang21","",
					"xuanxiang22","",
					"xuanxiang23","",
					"xuanxiang24","",
					"xuanxiang25","",
					"xuanxiang26","");
	$type = 2;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(尺度)
	@Options = ("5","left","right");
	$type = 3;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(数字题)
	@Options = ();
	$type = 4;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(文字题)
	@Options = ();
	$type = 5;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	


	
	##########
	#创建逻辑
	##########
	#($typename,$surveyid,$fromquest,$selected,$toquest)
	#目前只支持单个逻辑与条件,单题跳转单选项跳转,termination,jump
	my %logichash;
	%logichash = (
		'surveyId'=>$surveyid,
	);
	
	#一个逻辑
	$logichash{'logicList[0].conditions[0].optIds'}=1;
	$logichash{'logicList[0].conditions[0].qid'}=$questid[0];
	$logichash{'logicList[0].conditions[0].selected'}=1;
	$logichash{'logicList[0].result.destQid'}=$questid[$#questid];
	$logichash{'logicList[0].typeName'}='jump';

	#一个逻辑
	$logichash{'logicList[1].conditions[0].optIds'}=1;
	$logichash{'logicList[1].conditions[0].qid'}=$questid[0];
	$logichash{'logicList[1].conditions[0].selected'}=1;
	$logichash{'logicList[1].typeName'}='termination';

	#调试
	#while(my($k,$v) = each %logichash){
	#	print "debug---$k,$v\n";
	#}
	
	&editlogic($mgtbrowser,$mgt_addr,\%logichash);
	
	########
	#创建配额
	########
	#1女  0男
	my %quota;
	#提交一个
	%quota = (
		'age'=>"",
		'amount'=>"",
		'area'=>"",
		'sex'=>1,		#1.女   0.男
		'surveyCount'=>1,
		'surveyId'=>$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,\%quota);
	
	#提交一个
	%quota = (
		'age'=>"",
		'amount'=>"",
		'area'=>"",
		'sex'=>0,		#1.女   0.男
		'surveyCount'=>1,
		'surveyId'=>$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,\%quota);
	
	
	
	####
	#提交问卷
	####survey/a/submitSurvey?surveyId=3150
	&submit($mgtbrowser,$mgt_addr,$surveyid);
	
	####
	#审核上家
	####
	my %auditconfig = ();
	&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
	
	####
	#立刻上架操作
	####
	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","4");
	#print "debug--   update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid\n";
	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
	&operate_mysql(@survey,"$sql","update");
	sleep 1;
	
	
	
	

	#######
	#手机端验证是否存在该问卷
	#######
	
	#加载创建json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	

	#手机端的登录请求(登录没有验证身份证的账号)
	my ($mobrowser,$accessToken);
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[0]);	#男
	#($mobrowser,$accessToken) = &moblogin($mob_addr,$username[1]);	#男
	#($mobrowser,$accessToken) = &moblogin($mob_addr,$username[2]);	#女
	
	####
	#开始验证
	####
	my @verif;					#验证数组,全1才算通过
	my $temp = 0;
	for(1..30){		#每秒请求一次,30秒后返回没有需要的值,测试失败
		#type=4列出预报名问卷
		my $return = $mobrowser->get("$mob_addr/survey-ws/surveysvc/homepageList?sortType=2&pagesize=10&type=1&page=0&surveyid=-1",
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",)->content;
		&verification("mob","survey_IDverification.surveys",$mobrowser,$return);
		$json_obj = $json->decode("$return");	#把返回的json数组传入Json
		for my $surveylist (@{$json_obj->{'data'}->{'surveys'}}){
			#print "--debug-- $1\n";
			if(($surveylist->{'surveyId'} eq $surveyid)){
				#print "Found survey\n" . to_json($surveylist) ."\n";
				print "return value \"rest\":" . $surveylist->{'rest'}. "\n";
				print "return value \"restTime\":" . $surveylist->{'restTime'}. "\n";
				push @verif,1;
				$temp++,last;
			}
		}
		last if($temp);
		sleep 1;
	}
	unless($temp){
		print "无法找到上架问卷,测试异常\n";
		push @verif,0;
	}
	
	####
	#开始验证
	####
	#(登录第一个男性用户)开始答题,返回第一题的列表 
	my $return = $mobrowser->post("$mob_addr/survey-ws/surveysvc/seckill",
		{
		surveyid => $surveyid,
		addressid => '6377',
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
		)->content;
	#print "submit - $return\n";
	&verification("mob","survey_pic.surveys",$mobrowser,$return);
	
	$return = $json->decode("$return");

	if($return->{'data'}->{'isSucceed'} eq "true"){
		push @verif,1;
		print "isSucceed:" . $return->{'data'}->{'isSucceed'} . "  第一个男用户通过配额验证,测试通过\n";
	}else{
		push @verif,0;
		print "isSucceed:" . $return->{'data'}->{'isSucceed'} . "  第一个男用户没有通过配额验证,测试不通过\n";
	}
	
	print "*" x 10 ."\n";
	
	#(登录第二个男性用户)开始答题,返回第一题的列表 
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[1]);	#男
	
#	my $temp = 0;
#	for(1..30){		#每秒请求一次,30秒后返回没有需要的值,测试失败
#		#type=4列出预报名问卷
#		my $return = $mobrowser->get("$mob_addr/survey-ws/surveysvc/surveys?sortType=1&pagesize=10&type=1&page=0&surveyid=-1",
#			'Authorization'=> "Bearer $accessToken",
#			'Accept' => "application/json;charset=UTF-8",)->content;
#		&verification("mob","survey_IDverification.surveys",$mobrowser,$return);
#		$json_obj = $json->decode("$return");	#把返回的json数组传入Json
#		for my $surveylist (@{$json_obj->{'data'}->{'surveys'}}){
#			#print "--debug-- $1\n";
#			if($surveylist->{'surveyId'} eq $surveyid){
#				for my $invalidDetails (@{($surveylist->{'invalidDetails'})}){
#					if($invalidDetails->{'type'} eq "false"){
#						print "return value \"type\":" . $invalidDetails->{'type'}. "\n";
#						print "return value \"rest\":" . $surveylist->{'rest'}. "\n";
#						push @verif,1;
#						$temp++;
#						last;
#					}
#				}
#			}
#			last if($temp);
#		}
#		last if($temp);
#		sleep 1;
#	}
#	unless($temp){
#		print "第二个男性用户配额测试异常\n";
#		push @verif,0;
#	}
	
	my $return = $mobrowser->post("$mob_addr/survey-ws/surveysvc/seckill",
		{
		surveyid => $surveyid,
		addressid => '6377',
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
		)->content;
	#print "submit - $return\n";
	#&verification("mob","survey_pic.surveys",$mobrowser,$return);
	
	$return = $json->decode("$return");

	if($return->{'result'} eq "2"){
		push @verif,1;
		print "result:" . $return->{'result'} . "  配额已满无法答题,测试通过\n";
	}else{
		push @verif,0;
		print "result:" . $return->{'result'} . "  出现异常,测试不通过\n";
	}
	
	print "*" x 10 ."\n";

	#(登录第三个女性用户)开始答题,返回第一题的列表 
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[2]);	#女
	my $return = $mobrowser->post("$mob_addr/survey-ws/surveysvc/seckill",
		{
		surveyid => $surveyid,
		addressid => '6377',
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
		)->content;
	#print "submit - $return\n";
	&verification("mob","survey_pic.surveys",$mobrowser,$return);
	
	$return = $json->decode("$return");

	if($return->{'data'}->{'isSucceed'} eq "true"){
		push @verif,1;
		print "isSucceed:" . $return->{'data'}->{'isSucceed'} . "  第三个女用户通过配额验证,测试通过\n";
	}else{
		push @verif,0;
		print "isSucceed:" . $return->{'data'}->{'isSucceed'} . "  没有通过配额,测试不通过\n";
	}
	
	print "*" x 10 ."\n";
	
	#循环检查结果列表,并返回测试结果
	for(@verif){
		if($_ == 0){
			print "配额测试部分验证错误\n";
			return 0 ;
		}
	}
	print "配额测试全部验证正确\n";
	return 1;
	
}
1;