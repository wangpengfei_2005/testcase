#用户答题
#预置的答案 直接写入hash值

#在账号文件中编辑答案
#	|	分割题目
#	=	分割题号与答案
#	-	分割了一个范围值 可以在这个范围中随机 对于单选题来说只能随出一个值,对于多选题可以随出多个值
#	_	分割选项号以及该选项的文字叙述
#	timeout表示了超时


#use strict;

sub survey_answerFormMobile
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件
	my %data = &readExcel('1413188043_1000_survey_0305.xls','Sheet1');
	
	#被测试的问卷id
	my $surveyid = "69614";
	my $bingfa = 1;			#并发量
	my $answerCount = 1;	#每个用户答题次数

	
	#普通单线程模式
	#加载已经创建的用户列表
	my $count = 0;
	for my $index (0..$data{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		my $useraccount = $data{'account'}->[$index];	#用户账号
		my $myAnswerText = $data{'myAnswer'}->[$index];		#用户答题数据
		print "被测问卷id:$surveyid\n";
		
		#######
		#手机端模拟用户答题的请求
		#######
		
		#开始循环单个文件中的用户列表
		my %myAnswer = ();
		my @AnswerReturn;
		
		#处理用户答题数据将它存入hash
		if($myAnswerText =~ /^timeout$/){
			%myAnswer = ("timeout"=>"1");
		}else{
			for(split /\|/,$myAnswerText){
				my ($k,$v) = split /=/,$_;
				$myAnswer{$k} = $v;
			}
		}
		
		$count++;
		#%myAnswer = ("timeout"=>"1") if ($count <= 5);
		#%myAnswer = ("1"=>"1") if ($count > 50 and $count <= 150);
		#%myAnswer = ("1"=>"2-4") if ($count > 150);
		#my $r = &random(1,5);
		my $r = 1;
		
		for(1..$r){	#这里设置单个用户重复答题次数...
			@AnswerReturn = &userAnswerFromMobile($mob_addr,\@survey,$useraccount,$surveyid,\%myAnswer);
			next unless(@AnswerReturn);
			
		}
	}


	return -1;
}
1;