use strict;

#加载businessfunc/survey_sendMessage下的所有函数
my @filename = <businessfunc//survey_sendMessage//*.pl>;
require "$_" for(@filename);


sub survey_sendMessage_All
{
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");

	#被测用户账号
	my @username = &readLine('1407996376_5_survey_0305.txt');
	#开始获取被测用户id
	my @user_id;
	for(@username){
		my $sql = "SELECT USER_ID FROM `USER_ACCOUNT` WHERE LOGIN_NAME='$_';";
		push @user_id, &operate_mysql(@survey,"$sql","select");
	}


	
	
	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"yangxianming1");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#给所有用户发送消息
	########
	
	#testcase1 给所有用户发送消息;
	my $testcase = "sendMessageAll";
	#发送消息
	my $ms_tital = "yxmtital".time();	#消息标题,用时间戳来标记唯一
	my $ms_info = "yxminfo".time();	#消息内容
	print "ms_tital:$ms_tital\n";
	&sendmessage($mgtbrowser,$mgt_addr,$ms_info,0,-1,$ms_tital);
	#点击发送
	&clicksend($mgtbrowser,$mgt_addr,"",$ms_tital);
	
	#收消息并验证消息
	my @verif;	#验证数组,全1才算通过
	#手机端的登录请求
	my ($mobrowser,$accessToken);
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[0],'MTExMTExMTE=');
	#收消息并验证消息
	push @verif,&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase);
	#手机登录请求
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[1],'MTExMTExMTE=');
	#收消息并验证消息
	push @verif,&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase);
	#手机登录请求
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[2],'MTExMTExMTE=');
	#收消息并验证消息
	push @verif,&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase);
	
	
	
	#循环检查结果列表,并返回测试结果
	for(@verif){
		if($_ == 0){
			print "给全体用户发送消息,部分验证错误\n";
			return 0 ;
		}
	}
	print "给全体用户发送消息,测试全部验证正确\n";
	return 1;

}
1;