#没有city文件时，默认前6位是310103
#参数	($city_num,$date,$sex,$count)	都可以为空,然后就只返回一个id
#&create_ID("","","","");
#返回一组身份证号
#&create_ID("","","","");

use strict;

sub create_ID
{
	#读取数据(城市代码,出生年月yyyy-mm-dd,1男2女,生成数量)
	my ($city_num,$date,$sex,$count) = @_;
	my ($year,$month,$day);
	my @sex_1 = (1,3,5,7,9);	#男标识
	my @sex_2 = (0,2,4,6,8);	#女
	my $shunxuma = 0;	#顺序代码初始值
	my $sexCode;
	my @idList;			#用户返回生成的身份证
	
	#随机值控制 默认都做随机操作
	my $cityRand_contorl = 1;
	my $dateRand_contorl = 1;
	my $sexRand_contorl = 1;
	$cityRand_contorl = 0 if($city_num);
	$dateRand_contorl = 0 if($date);
	$sexRand_contorl = 0 if($sex);
	
	#count给予默认值
	$count = 1 unless($count);
	
	for(1..$count){
		my $id = "";		#存放身份证号
		my @idArr = (); 	#用于计算第18位
		my $yushu;	#计算17位的余数
		my $n18 = 0;	#第18位
		
		#城市代码如果是空就从resources/city中随机选出一个城市
		if($cityRand_contorl){
			$city_num = &fun_city();
		}
		
		#日期随机值控制
		if($dateRand_contorl){
			#随机选出year month day
			$year = int (1950+rand 60);
			$month = int (1+rand 12);
			$day = int (1+rand 28);
			$date = $year."-".$month."-".$day;
		}
		#格式化日期
		($year,$month,$day) = split /-/,$date;
		$month = sprintf "%02d",$month;
		$day = sprintf "%02d",$day;
		
		#循序码
		$shunxuma = int (0+rand 99);
		$shunxuma = sprintf "%02d",$shunxuma;

		#性别
		$sexCode = int (0+rand 9) if($sexRand_contorl);	#男女
		$sexCode = $sex_1[int (0+rand $#sex_1)] if($sex == 1);	#男
		$sexCode = $sex_2[int (0+rand $#sex_2)] if($sex == 2);	#女

		#组装前17位
		$id = $city_num.$year.$month.$day.$shunxuma.$sexCode;
		
		#计算第18位
		@idArr = split //,$id;	
		$yushu = (	$idArr[0]*7+
					$idArr[1]*9+
					$idArr[2]*10+
					$idArr[3]*5+
					$idArr[4]*8+
					$idArr[5]*4+
					$idArr[6]*2+
					$idArr[7]*1+
					$idArr[8]*6+
					$idArr[9]*3+
					$idArr[10]*7+
					$idArr[11]*9+
					$idArr[12]*10+
					$idArr[13]*5+
					$idArr[14]*8+
					$idArr[15]*4+
					$idArr[16]*2
		)%11;
		
		$n18 = 1 if( $yushu == 0 );
		$n18 = 0 if( $yushu == 1 );
		$n18 = "x" if( $yushu == 2 );
		$n18 = 9 if( $yushu == 3 );
		$n18 = 8 if( $yushu == 4 );
		$n18 = 7 if( $yushu == 5 );
		$n18 = 6 if( $yushu == 6 );
		$n18 = 5 if( $yushu == 7 );
		$n18 = 4 if( $yushu == 8 );
		$n18 = 3 if( $yushu == 9 );
		$n18 = 2 if( $yushu == 10 );
		$id = $id.$n18;
		
		#塞入列表
		push @idList,$id;
	}
	
	return @idList;
	
	
	sub fun_city{
		unless (open cFILE,"resources\\city"){
			print "Can not open city file!!I will return this cityID ,310103.\n";
			return 310103;
			}
		my @city_id = <cFILE>;
		close cFILE;
		
		for(@city_id){
			$_ = $1 if(/(\d+)/);
		}
		
		#随机选出cityid
		my $cityIndex = int (0+rand $#city_id);
		
		return $city_id[$cityIndex];
	}
}
1;