#读取Data目录下的一个文件，然后返回读取的数据
#本函数用到了动态变量 所以无法使用use strict
#my %data = &readData("1.txt",',');
#print $data{'age'} -> [1] . "\n";

sub readData
{	
	#获取文件名，还有分隔符
	my ($filename, $symbol) = @_;
	my %Datahash;

	#验证文件是否存在,并读取
	unless(open Data,"data\\$filename"){
		print "Can not open file $filename\n";
		return 0;
	}
	my @readline;
	for my $line (<Data>){
		chomp($line);				#去掉换行符
		next if($line =~ /^#/);		#去掉注释
		next if($line =~ /^\s+/);	#去掉空行
		next if($line eq "");		#去掉空行
		push @readline,$line;	
	}
	
	#获取数据文件的最大数据行不算field行
	my $maxDataLine = @readline - 2;
	$Datahash{'maxDataLine'} = $maxDataLine;

	#$symbol如果为空，那就不做字段分割 直接插入Datahash表并返回
	unless($symbol){
		my $k = shift @readline;
		$Datahash{$k} = \@readline;
		return %Datahash;
	}
	
	#用预置的分隔符解释文件
	#通过第一行获取字段信息
	#/\Q$symbol\E/ 为转义引用值 
	my @field = split /\Q$symbol\E/,$readline[0];

	#通过动态变量名给不同的hash的key初始化数组引用的value
	$Datahash{$_} = \@{'saveValue'.$_} for(@field);

	#传入从第二行开始的数据
	my @splitLine;
	for(my $n=1; $n<=$#readline; $n++){
		@splitLine = split /\Q$symbol\E/,$readline[$n];
		for(my $m=0; $m<=$#field; $m++){
			push $Datahash{$field[$m]}, $splitLine[$m];
		}
	}
	
	#测试代码
#	while(my($k,$v) = each %Datahash){
#		print "$k ";
#		for(@$v){
#			print "$_ ";
#		}
#		print "\n";
#	}
	
	return %Datahash;
	
}


1;