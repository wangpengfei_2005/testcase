#用xpath来找到对应的节点值

sub xpath_find_value
{
	my ($html,$xpath_word) = @_;
	my $tree = new HTML::TreeBuilder::XPath;
	$tree->parse("$html");
	$tree->eof;
	
	#验证xpath路径是否正确或存在
	unless( $tree->exists($xpath_word)){
		print "warning: xpath路径-> $xpath_word <- 不存在\n";
		return 0 
	}
	
	my @items = $tree->findnodes_as_strings( $xpath_word );
	for(@items){
		#转码处理,可以直接在cmd现实正确的中文
		$_ = encode("gbk", decode("utf-8", $_));
	}
	
	return $items[0] if ($#items == 0);
	return @items;
}


1;